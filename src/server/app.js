const Koa = require('koa');
const serve = require('koa-static');
const mount = require("koa-mount");
const bodyParser = require('koa-bodyparser');
const formidable = require('koa2-formidable');
const cors = require('koa2-cors');
const logger = require('koa-logger');
const https = require('https');
const fs = require('fs');
const { default: enforceHttps } = require('koa-sslify');

const appBack = module.exports = new Koa();

appBack.use(logger());

appBack.use(cors({
    credentials: true
}));

appBack.use(mount('/img', serve('./img')));

appBack.use(formidable({multiples: true}));
appBack.use(bodyParser());

const authRoutes = require('./routes/auth');
appBack.use(authRoutes.routes());

const registrationRoutes = require('./routes/registration');
appBack.use(registrationRoutes.routes());

const usersRoutes = require('./routes/users');
appBack.use(usersRoutes.routes());

const productsRoutes = require('./routes/products');
appBack.use(productsRoutes.routes());

const ordersRoutes = require('./routes/orders');
appBack.use(ordersRoutes.routes());

const marketsRoutes = require('./routes/markets');
appBack.use(marketsRoutes.routes());

const faqRoutes = require('./routes/faq');
appBack.use(faqRoutes.routes());

const feedbackRoutes = require('./routes/feedback');
appBack.use(feedbackRoutes.routes());

const infoRoutes = require('./routes/info');
appBack.use(infoRoutes.routes());

const categoriesRoutes = require('./routes/categories');
appBack.use(categoriesRoutes.routes());

const deliveryRoutes = require('./routes/delivery');
appBack.use(deliveryRoutes.routes());

const payRoutes = require('./routes/pay');
appBack.use(payRoutes.routes());

let portBack = 13579;

//const serverBack = appBack.listen(portBack, () => {
//    console.log(`Server backend listening on port: ${portBack}`);
//});

appBack.use(enforceHttps({
    port: portBack
}));

let options = {
    key: fs.readFileSync('/etc/letsencrypt/live/farmer-market.ru/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/farmer-market.ru/fullchain.pem')
}

https.createServer(options, appBack.callback()).listen(portBack, () => {
   console.log(`Https server backend listening on port: ${portBack}`);
});

module.exports = {

};

