const fs = require('fs');
const hash = require('object-hash');
const productQueries = require('../db/queries/products');

const addImage = async (ctx, next) => {

    let image = ctx.request.files.image;
    if (image) {
        try {
            let name = hash({name: image.name, date: new Date()});
            let imgPath = 'img/' + name + '.jpg';
            const reader = fs.createReadStream(image.path);
            const writer = fs.createWriteStream(imgPath);
            // console.log(writer);
            ctx.request.body.image = imgPath;
            await reader.pipe(writer);

        } catch {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Ошибка загрузки изображения'
            };
            return;
        }
    } else {
        ctx.request.body.image = null;
    }
    return next();
};

const updateImage = async (ctx, next) => {

    let image = ctx.request.files.image;
    if (image) {
        try {
            let oldImg = await productQueries.getProductImage(ctx.request.body.id);
            if (oldImg) await fs.unlink(oldImg, (res) => res);
            let name = hash({name: image.name, date: new Date()});
            let imgPath = 'img/' + name + '.jpg';
            const reader = fs.createReadStream(image.path);
            const writer = fs.createWriteStream(imgPath);
            ctx.request.body.image = imgPath;
            await reader.pipe(writer);

        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Ошибка загрузки изображения'
            };
            console.log(err);
            return;
        }
    }
    return next();
};

module.exports = {
    addImage,
    updateImage
};
