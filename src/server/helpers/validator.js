const BaseJoi = require('joi');
const ImageExtension = require('joi-image-extension');
const Joi = BaseJoi.extend(ImageExtension);

let validate = (schema) =>
    (ctx, next) => {
        let res = Joi.validate(ctx.request.body, schema);
        if (res.error !== null) {
            console.log(res.error);
            ctx.status = 400;
            ctx.body = {
                status: 'error',
                message: 'Введены некорректные данные!'
            };
            return;
        }
        return next();
    };

const USER_REGISTRATION_SCHEMA = Joi
    .object()
    .keys({
        email: Joi.string(),
        telephone: Joi.string().regex(/^\d+$/).min(10),
        password: Joi.string().required(),
        first_name: Joi.string(),
        second_name: Joi.string(),
        subscription: Joi.boolean().allow(null)
    })
    .or('email', 'telephone');

const GET_USER_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1),
        email: Joi.string().email(),
        telephone: Joi.string().regex(/^\d+$/).min(10)
    });

const EDIT_PROFILE_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required(),
        data: Joi.object().keys({
            email: Joi.string().email(),
            telephone: Joi.string().regex(/^\d+$/).min(10),
            password: Joi.string(),
            first_name: Joi.string().allow(null, ''),
            second_name: Joi.string().allow(null, ''),
            postcode: Joi.string().length(6).allow(null, ''),
            birth_date: Joi.number().integer().allow(null),
            country: Joi.string().allow(null, ''),
            region: Joi.string().allow(null, ''),
            city: Joi.string().allow(null, ''),
            address: Joi.string().allow(null, ''),
            subscription: Joi.boolean().allow(null)
        }).or('email', 'telephone', 'password', 'first_name', 'second_name', 'postcode', 'birth_date', 'country', 'region', 'city', 'address', 'subscription').required()
    });

const AUTH_SCHEMA = Joi
    .object()
    .keys({
        login: [Joi.string().regex(/^\d+$/).min(10), Joi.string().email()],
        password: Joi.string().min(6)
    })
    .required('login', 'password');

const DESTROY_TOKENS_SCHEMA = Joi
    .object()
    .keys({});

const CHECK_RECOVERY_HASH_SCHEMA = Joi
    .object()
    .keys({
        email: Joi.string().email().required(),
        hash: Joi.string().required()
    });

const SEND_RECOVERY_HASH_SCHEMA = Joi
    .object()
    .keys({
        email: Joi.string().email().required()
    });

const SET_NEW_PASSWORD = Joi
    .object()
    .keys({
        email: Joi.string().email().required(),
        hash: Joi.string().required(),
        password: Joi.string().min(6).required()
    });

const CHECK_SCHEMA = Joi
    .object()
    .keys({});

const GET_ALL_CATEGORIES_SCHEMA = Joi
    .object()
    .keys({
        forEdit: Joi.boolean(),
    });

const ADD_CATEGORY_SCHEMA = Joi
    .object()
    .keys({
        name: Joi.string().required()
    });

const GET_ONE_CATEGORY_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const EDIT_CATEGORY_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required(),
        name: Joi.string().required()
    });

const DELETE_CATEGORY_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const GET_FAQ_SCHEMA = Joi
    .object()
    .keys({});

const GET_ONE_FAQ_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const GET_FAQ_BY_TOPIC_SCHEMA = Joi
    .object()
    .keys({
        topic: Joi.number().integer().min(1).required()
    });

const GET_FAQ_TOPICS_SCHEMA = Joi
    .object()
    .keys({});

const ADD_FAQ_SCHEMA = Joi
    .object()
    .keys({
        topic: Joi.number().integer().min(1).required(),
        question: Joi.string().required(),
        answer: Joi.string().required()
    });

const ADD_FAQ_TOPIC_SCHEMA = Joi
    .object()
    .keys({
        topic: Joi.string().required()
    });

const EDIT_FAQ_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required(),
        topic: Joi.number().integer().min(1),
        question: Joi.string(),
        answer: Joi.string()
    });

const EDIT_FAQ_TOPIC_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required(),
        topic: Joi.string().required()
    });

const DELETE_FAQ_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const DELETE_FAQ_TOPIC_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const SEND_MESSAGE_SCHEMA = Joi
    .object()
    .keys({
        name: Joi.string().required(),
        email: Joi.string().email().required(),
        telephone: Joi.string().regex(/^\d+$/).min(10).required(),
        message: Joi.string().required()
    });

const GET_MESSAGES_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer().min(0),
        offset: Joi.number().integer().min(0)
    });

const GET_MESSAGE_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const DELETE_MESSAGE_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const GET_INFO_SCHEMA = Joi
    .object()
    .keys({
        topic: Joi.string().required()
    });

const ADD_INFO_SCHEMA = Joi
    .object()
    .keys({
        topic: Joi.string().required(),
        name: Joi.string().required(),
        text: Joi.string().required()
    });

const EDIT_INFO_SCHEMA = Joi
    .object()
    .keys({
        topic: Joi.string().required(),
        name: Joi.string(),
        text: Joi.string()
    });

const DELETE_INFO_SCHEMA = Joi
    .object()
    .keys({
        topic: Joi.string().required()
    });

const GET_TOPICS_INFO_SCHEMA = Joi
    .object()
    .keys({});

const GET_MANY_MARKETS_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer(),
        offset: Joi.number().integer()
    });

const GET_ONE_MARKET_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().required()
    });

const ADD_MARKET_SCHEMA = Joi
    .object()
    .keys({
        name: Joi.string().required(),
        country: Joi.string().required(),
        region: Joi.string().required(),
        city: Joi.string().required(),
        address: Joi.string().required(),
        geoposition: Joi.array().items(Joi.number()).length(2).required(),
        timetable: Joi.string().allow('', null).required(),
        email: Joi.string().email().allow('', null).required(),
        telephones: Joi.array().items(Joi.string().regex(/^\d+$/).min(10).allow('', null)).min(0).max(5).required(),
        payment: Joi.array().items(Joi.string().valid('cash', 'cashless').allow('', null)).min(0).max(2),
        ym_number: Joi.string().regex(/^\d+$/),
        delivery: Joi.array().items(Joi.string().valid('pickup', 'regions_delivery').allow('', null)).min(0).max(3),
        allowed_delivery_addresses: Joi.array().items(Joi.string()),
        active: Joi.boolean()
    });

const EDIT_MARKET_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().required(),
        data: Joi.object().keys({
            name: Joi.string(),
            country: Joi.string(),
            region: Joi.string(),
            city: Joi.string(),
            address: Joi.string(),
            geoposition: Joi.array().items(Joi.number()).length(2),
            timetable: Joi.string().allow('', null),
            email: Joi.string().email().allow('', null),
            telephones: Joi.array().items(Joi.string().regex(/^\d+$/).min(10).allow('', null)).min(0).max(5),
            payment: Joi.array().items(Joi.string().valid('cash', 'cashless').allow('', null)).min(0).max(2),
            ym_number: Joi.string().regex(/^\d+$/),
            delivery: Joi.array().items(Joi.string().valid('pickup', 'regions_delivery').allow('', null)).min(0).max(3),
            allowed_delivery_addresses: Joi.array().items(Joi.string()),
            active: Joi.boolean()
        })
    });

const GET_REPORT_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        fromTime: Joi.number().integer().min(1),
        toTime: Joi.number().integer().min(Joi.ref('fromTime')),
        orderBy: Joi.string().allow('', null),
        order: Joi.string().allow('', null),
        sendToEmail: Joi.boolean()
    });

const ADD_MANAGER_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        manager: Joi.number().integer().min(1).required()
    });

const DELETE_MANAGER_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        manager: Joi.number().integer().min(1).required()
    });

const GET_MANAGERS_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required()
    });

const GET_ORDERS_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer().min(1),
        offset: Joi.number().integer().min(0),
        status: Joi.number().integer().min(0)
    });

const GET_ORDER_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const GET_SUBORDER_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.string().regex(/^\d+-\d+$/).min(3).required()
    });

const ADD_ORDER_SCHEMA = Joi
    .object()
    .keys({
        suborders: Joi.array().items(Joi.object().keys({
                product: Joi.number().integer().min(1).required(),
                count: Joi.number().integer().min(1).required(),
                cost: Joi.number().integer().min(1).required()
            })
        ),
        type: Joi.string().allow('WHOLESALE', 'RETAIL').required(),
        pay_type: Joi.string().allow('CASH', 'CASHLESS').required(),
        delivery_type: Joi.string().allow('pickup', 'regions_delivery').required(),
        delivery_address: Joi.string()
    });

const GET_MARKET_SUBORDERS_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        fromTime: Joi.number().integer().min(1),
        toTime: Joi.number().integer().min(Joi.ref('fromTime')),
        limit: Joi.number().integer().min(0),
        offset: Joi.number().integer().min(0),
        orderBy: Joi.string().allow('', null),
        order: Joi.string().allow('', null),
        report: Joi.boolean()
    });

const CANCEL_ORDER_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const CANCEL_SUBORDER_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.string().regex(/^\d+-\d+$/).min(3).required()
    });

const SET_SUBORDER_STATUS_SCHEMA =Joi
    .object()
    .keys({
        id: Joi.string().regex(/^\d+-\d+$/).min(3).required(),
        status: Joi.number().integer().min(1).max(5).required()
    });

const GET_ONE_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const GET_MANY_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer().min(1),
        offset: Joi.number().integer().min(0),
        orderBy: Joi.any(), //[Joi.string().allow('', null), Joi.array().items(Joi.string().allow('', null))],
        order: Joi.any(), //[Joi.string().allow('', null), Joi.array().items(Joi.string().allow('', null))],
        market: Joi.number().integer().min(1),
        category: Joi.number().integer().min(1),
        admin: Joi.boolean()
    });

const GET_BY_MARKET_ID_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer().min(1),
        offset: Joi.number().integer().min(0),
        orderBy: Joi.string().allow('', null),
        order: Joi.string().allow('', null),
        market: Joi.number().integer().min(1)
    });

const GET_BY_MY_MARKETS_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer().min(1),
        offset: Joi.number().integer().min(0),
        orderBy: Joi.string().allow('', null),
        order: Joi.string().allow('', null),
        market: Joi.number().integer().min(1),
        onlyActive: Joi.boolean()
    });

const GET_TOP_SCHEMA = Joi
    .object()
    .keys({});

const GET_NEW_SCHEMA = Joi
    .object()
    .keys({});

const FIND_SCHEMA = Joi
    .object()
    .keys({
        searchQuery: Joi.string().required(),
        limit: Joi.number().integer().min(1),
        offset: Joi.number().integer().min(0),
        orderBy: Joi.string().allow('', null),
        order: Joi.string().allow('', null),
        type: Joi.string().allow('retail', 'wholesale')
    });

const ADD_PRODUCT_SCHEMA = Joi
    .object()
    .keys({
        name: Joi.string().required(),
        category: Joi.number().integer().min(1).required(),
        sort: Joi.string().allow('Высший', '1', '2', '3').required(),
        description: Joi.string().required(),
        limit: Joi.number().integer().min(0).required(),
        image: Joi.any(),
        market: Joi.number().integer().min(1).required(),
        active: Joi.boolean(),
        wholesale: Joi.boolean(),
        retail: Joi.boolean(),
        retail_price: Joi.number().integer().min(0)
    });

const EDIT_PRODUCT_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required(),
        name: Joi.string(),
        category: Joi.number().integer().min(1),
        sort: Joi.string().allow('Высший', '1', '2', '3'),
        description: Joi.string(),
        limit: Joi.number().integer().min(0),
        image: Joi.any(),
        market: Joi.number().integer().min(1),
        active: Joi.boolean(),
        wholesale: Joi.boolean(),
        retail: Joi.boolean(),
        retail_price: Joi.number().integer().min(0)
    });

const FIND_USER_SCHEMA = Joi
    .object()
    .keys({
        searchQuery: Joi.string().required()
    });

const GET_MY_MARKETS_SCHEMA = Joi
    .object()
    .keys({
        onlyOwner: Joi.boolean(),
        onlyActive: Joi.boolean()
    });

const GET_MANAGER_NOTIFICATIONS_SCHEMA = Joi
    .object()
    .keys({

    });

const SET_MANAGER_NOTIFICATIONS_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        notifications: Joi.boolean().required()
    });

const CHANGE_PASSWORD_SCHEMA = Joi
    .object()
    .keys({
        oldPassword: Joi.string().required(),
        newPassword1: Joi.string().required(),
        newPassword2: Joi.string().required()
    });

const SET_CAN_EDIT_PRODUCTS_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        manager: Joi.number().integer().min(1).required(),
        canEditProducts: Joi.boolean().required()
    });

const GET_ALL_USERS_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer().min(1),
        offset: Joi.number().integer().min(0),
        orderBy: Joi.string().allow('', null),
        order: Joi.string().allow('', null)
    });

const GET_STATUS_LIST_SCHEMA = Joi
    .object()
    .keys({});

const GET_FARMERS_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer().min(1),
        offset: Joi.number().integer().min(0),
        orderBy: Joi.string().allow('', null),
        order: Joi.string().allow('', null)
    });

const GET_FARMER_REQUESTS_SCHEMA = Joi
    .object()
    .keys({
        limit: Joi.number().integer().min(1),
        offset: Joi.number().integer().min(0),
        orderBy: Joi.string().allow('', null),
        order: Joi.string().allow('', null)
    });

const SET_IS_FARMER_SCHEMA = Joi
    .object()
    .keys({
        userId: Joi.number().integer().min(1).required(),
        isFarmer: Joi.boolean().required()
    });

const SET_BECOME_FARMER_SCHEMA = Joi
    .object()
    .keys({
        becomeFarmer: Joi.boolean().required()
    });

const ADD_ADDRESS_SCHEMA = Joi
    .object()
    .keys({
        region: Joi.string().required(),
        locality: Joi.string().required(),
        street: Joi.string().required(),
        house: Joi.string().required(),
        apartment: Joi.string().required(),
        first_name: Joi.string().required(),
        second_name: Joi.string().required(),
        phone_number: Joi.string().required(),
        email: Joi.string().required()
    });

const GET_MY_ADDRESSES_SCHEMA = Joi
    .object()
    .keys({

    });

const EDIT_ADDRESS_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required(),
        region: Joi.string(),
        locality: Joi.string(),
        street: Joi.string(),
        house: Joi.string(),
        apartment: Joi.string(),
        first_name: Joi.string(),
        second_name: Joi.string(),
        phone_number: Joi.string(),
        email: Joi.string()
    })
    .min(2);

const DELETE_ADDRESS_SCHEMA = Joi
    .object()
    .keys({
        id: Joi.number().integer().min(1).required()
    });

const GET_PAYMENT_DATA_SCHEMA = Joi
    .object()
    .keys({
        orderId: Joi.number().integer().min(1).required()
    });

const ADD_COURIER_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        courier: Joi.number().integer().min(1).required()
    });

const DELETE_COURIER_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        courier: Joi.number().integer().min(1).required()
    });

const GET_COURIERS_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required()
    });

const GET_COURIER_NOTIFICATIONS_SCHEMA = Joi
    .object()
    .keys({

    });

const SET_COURIER_NOTIFICATIONS_SCHEMA = Joi
    .object()
    .keys({
        market: Joi.number().integer().min(1).required(),
        notifications: Joi.boolean().required()
    });

const GET_MANY_BY_IDS_SCHEMA = Joi
    .object()
    .keys({
        ids: Joi.array().items(Joi.number().integer()).required()
    });

module.exports = {
    validate,
    USER_REGISTRATION_SCHEMA,
    GET_USER_SCHEMA,
    EDIT_PROFILE_SCHEMA,
    AUTH_SCHEMA,
    DESTROY_TOKENS_SCHEMA,
    CHECK_RECOVERY_HASH_SCHEMA,
    SEND_RECOVERY_HASH_SCHEMA,
    SET_NEW_PASSWORD,
    CHECK_SCHEMA,
    GET_ALL_CATEGORIES_SCHEMA,
    ADD_CATEGORY_SCHEMA,
    GET_ONE_CATEGORY_SCHEMA,
    EDIT_CATEGORY_SCHEMA,
    DELETE_CATEGORY_SCHEMA,
    GET_FAQ_SCHEMA,
    GET_ONE_FAQ_SCHEMA,
    GET_FAQ_BY_TOPIC_SCHEMA,
    GET_FAQ_TOPICS_SCHEMA,
    ADD_FAQ_SCHEMA,
    ADD_FAQ_TOPIC_SCHEMA,
    EDIT_FAQ_SCHEMA,
    EDIT_FAQ_TOPIC_SCHEMA,
    DELETE_FAQ_SCHEMA,
    DELETE_FAQ_TOPIC_SCHEMA,
    SEND_MESSAGE_SCHEMA,
    GET_MESSAGES_SCHEMA,
    GET_MESSAGE_SCHEMA,
    DELETE_MESSAGE_SCHEMA,
    GET_INFO_SCHEMA,
    ADD_INFO_SCHEMA,
    EDIT_INFO_SCHEMA,
    DELETE_INFO_SCHEMA,
    GET_TOPICS_INFO_SCHEMA,
    GET_MANY_MARKETS_SCHEMA,
    GET_ONE_MARKET_SCHEMA,
    ADD_MARKET_SCHEMA,
    EDIT_MARKET_SCHEMA,
    GET_REPORT_SCHEMA,
    ADD_MANAGER_SCHEMA,
    DELETE_MANAGER_SCHEMA,
    GET_MANAGERS_SCHEMA,
    GET_ORDERS_SCHEMA,
    GET_ORDER_SCHEMA,
    GET_SUBORDER_SCHEMA,
    ADD_ORDER_SCHEMA,
    GET_MARKET_SUBORDERS_SCHEMA,
    CANCEL_ORDER_SCHEMA,
    CANCEL_SUBORDER_SCHEMA,
    SET_SUBORDER_STATUS_SCHEMA,
    GET_ONE_SCHEMA,
    GET_MANY_SCHEMA,
    GET_BY_MARKET_ID_SCHEMA,
    GET_BY_MY_MARKETS_SCHEMA,
    GET_TOP_SCHEMA,
    GET_NEW_SCHEMA,
    FIND_SCHEMA,
    ADD_PRODUCT_SCHEMA,
    EDIT_PRODUCT_SCHEMA,
    FIND_USER_SCHEMA,
    GET_MY_MARKETS_SCHEMA,
    GET_MANAGER_NOTIFICATIONS_SCHEMA,
    SET_MANAGER_NOTIFICATIONS_SCHEMA,
    CHANGE_PASSWORD_SCHEMA,
    SET_CAN_EDIT_PRODUCTS_SCHEMA,
    GET_ALL_USERS_SCHEMA,
    GET_STATUS_LIST_SCHEMA,
    GET_FARMERS_SCHEMA,
    GET_FARMER_REQUESTS_SCHEMA,
    SET_IS_FARMER_SCHEMA,
    SET_BECOME_FARMER_SCHEMA,
    ADD_ADDRESS_SCHEMA,
    GET_MY_ADDRESSES_SCHEMA,
    EDIT_ADDRESS_SCHEMA,
    DELETE_ADDRESS_SCHEMA,
    GET_PAYMENT_DATA_SCHEMA,
    ADD_COURIER_SCHEMA,
    DELETE_COURIER_SCHEMA,
    GET_COURIERS_SCHEMA,
    GET_COURIER_NOTIFICATIONS_SCHEMA,
    SET_COURIER_NOTIFICATIONS_SCHEMA,
    GET_MANY_BY_IDS_SCHEMA
};



