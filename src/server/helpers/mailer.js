const nodemailer = require('nodemailer');
const axios = require('axios');
const moment = require('moment');
const userQueries = require('../db/queries/users');
const marketsQueries = require('../db/queries/markets');
const ordersQueries = require('../db/queries/orders');
const productsQueries = require('../db/queries/products.js');

let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
        user: "farmermarket.information",
        pass: "VfufpbyJuehwjd1967"
    }
});

async function getShortLink(url) {
    let test = await axios
        .get('https://clck.ru/--', { params: { url: url }} )
        .then(res => res.data);
    return test;
}

async function sendRecoveryCode(to, token) {
    let link = await getShortLink(`http://farmer-market.ru/recovery?email=${to}&token=${token}`);
    let info = await transporter.sendMail({
        from: '"Farmer Market"<farmermarket.information@gmail.com>', // sender address
        to: to,
        subject: "Восстановление пароля", // Subject line
        html:   `Это сообщение сгенерировано, потому что кто-то указал Ваш адрес электронной почты` +
            ` для восстановления доступа к сайту Farmer Market. Для продолжения перейдите по ` +
            `<a href = "${link}">ссылке</a>. Если Вы не понимаете о ` +
            `чем идет речь, просто проигнорируйте это сообщение.`
    });
}

async function sendReport(to, data) {
    let info = await transporter.sendMail({
        from: '"Farmer Market"<farmermarket.information@gmail.com>',
        to: to,
        subject: `Отчет по фермерскому хозяйству "${data.market}"`,
        html:   `Добрый день! \nЗаказанный отчет по фермерскому хозяйству <b>"${data.market}"</b> за период: ` +
            `${data.fromTime} - ${data.toTime} находится во вложении письма.`,
        attachments: [
            {
                filename: `Отчет "${data.market}" (${data.creationTime}).pdf`,
                path: data.pdfData
            }
        ]
    });
}


async function sendNewOrderNotification(data) {


    let customer = await userQueries.getUser({id: data.customer});
    let dateTime = await moment.unix(data.created_at).utcOffset(3 * 60).locale('ru').format('MMMM DD YYYY, HH:mm');

    let sorted = data.suborders.reduce((result, suborder) => {
        const a = result.find(({market}) => market === suborder.product.market);
        a ? a.suborders.push(suborder) : result.push({market: suborder.product.market, suborders: [suborder]});
        return result;
    }, []);

    for await (order of sorted) {
        let market = await marketsQueries.getOne(order.market);
        let managersEmail = await marketsQueries.getManagersEmail(order.market);
        let subject = `Новый заказ в фермерском хозяйстве ${market.name}`;
        if (managersEmail.length) {

            let message = `<h3>Новый заказ в фермерском хозяйстве ${market.name}!</h3><br/>` +
                `<b>Время формирования заказа:</b> ${dateTime}<br/>` +
                `<b>Заказчик:</b> ${customer.first_name} ${customer.second_name}<br/>` +
                `<b>Номер телефона:</b> ${customer.telephone}<br/>` +
                `<b>Продукты:</b><br/><br/>`;

            message += '<ol>';

            for (let i = 0; i < order.suborders.length; i++) {
                let suborder = order.suborders[i];
                message += `<li><b>${suborder.product.name}. ${suborder.product.sort} сорт.</b></li>`;
                message += `<ul><li><b>Количество:</b> ${suborder.count} кг</li><li><b>Цена:</b> ${suborder.cost} руб/кг</li></ul></li>`;
            }

            message += '</ol>';

            await sendEmail(managersEmail, subject, message);
        }
    }
}

async function sendOrderIsCancelledNotification(data) {


    let customer = await userQueries.getUser({id: data.customer});

    let sorted = data.suborders.reduce((result, suborder) => {
        const a = result.find(({market}) => market === suborder.product.market);
        a ? a.suborders.push(suborder) : result.push({market: suborder.product.market, suborders: [suborder]});
        return result;
    }, []);

    for await (order of sorted) {
        let market = await marketsQueries.getOne(order.market);
        let managersEmail = await marketsQueries.getManagersEmail(order.market);
        let subject = `Отмена заказа в фермерском хозяйстве ${market.name}`;
        if (managersEmail.length) {

            let message = `<h3>Отмена заказа № ${order.id} в фермерском хозяйстве ${market.name}!</h3><br/>` +
                `<b>Заказчик:</b> ${customer.first_name} ${customer.second_name}<br/>` +
                `<b>Номер телефона:</b> ${customer.telephone}<br/>` +
                `<b>Продукты:</b><br/><br/>`;

            message += '<ol>';

            for (let i = 0; i < order.suborders.length; i++) {
                let suborder = order.suborders[i];
                message += `<li><b>${suborder.product.name}. ${suborder.product.sort} сорт.</b></li>`;
                message += `<ul><li><b>Количество:</b> ${suborder.count} кг</li><li><b>Цена:</b> ${suborder.cost} руб/кг</li></ul></li>`;
            }

            message += '</ol>';

            await sendEmail(managersEmail, subject, message);
        }
    }
}

async function sendSuborderIsCancelledNotification(id) {


    let order = await ordersQueries.getOrderBySuborder(id);

    let suborder = await ordersQueries.getSuborder(id);

    let product = (await productsQueries.getOneById(0, suborder.product)).product;

    let customer = await userQueries.getUser({id: order.customer});

    let managersEmail = await marketsQueries.getManagersEmail(product.market.id);

    let subject = `Отмена подзаказа в фермерском хозяйстве ${product.market.name}`;

    if (managersEmail.length) {

        let message = `<h3>Отмена подзаказа № ${order.id}-${suborder.id} в фермерском хозяйстве ${product.market.name}!</h3><br/>` +
            `<b>Заказчик:</b> ${customer.first_name} ${customer.second_name}<br/>` +
            `<b>Номер телефона:</b> ${customer.telephone}<br/>` +
            `<b>Продукт:</b><br/><br/>`;

        message += '<ol>';
        message += `<li><b>${product.name}. ${product.sort} сорт.</b></li>`;
        message += `<ul><li><b>Количество:</b> ${suborder.count} кг</li><li><b>Цена:</b> ${suborder.cost} руб/кг</li></ul></li>`;
        message += '</ol>';

        await sendEmail(managersEmail, subject, message);
    }
}

async function sendNewSuborderStatus(id) {


    let order = await ordersQueries.getOrderBySuborder(id);

    let suborder = await ordersQueries.getSuborder(id);

    let product = (await productsQueries.getOneById(0, suborder.product)).product;

    let customer = await userQueries.getUser({id: order.customer});

    let subject = `Изменения статуса подзаказа в фермерском хозяйстве ${product.market.name}`;

    if (customer.notifications) {

        let message = `<h3>Изменение статуса подзаказа № ${order.id}-${suborder.id} в фермерском хозяйстве ${product.market.name}!</h3><br/>` +
            `<b>Заказчик:</b> ${customer.first_name} ${customer.second_name}<br/>` +
            `<b>Номер телефона:</b> ${customer.telephone}<br/>` +
            `<b>Продукт:</b><br/><br/>`;

        message += '<ol>';
        message += `<li><b>${product.name}. ${product.sort} сорт.</b></li>`;
        message += `<ul><li><b>Количество:</b> ${suborder.count} кг</li><li><b>Цена:</b> ${suborder.cost} руб/кг</li></ul></li>`;
        message += '</ol>';
        message += `<br/><br/>Статус подзаказа изменен на: <b>${suborder.status}</b>.`;

        await sendEmail(managersEmail, subject, message);
    }
}

async function sendEmail(to, subject, message) {
    let res = await transporter.sendMail({
        from: '"Farmer Market"<farmermarket.information@gmail.com>',
        to: to,
        subject: subject,
        html:   message
    });

    // console.log(res);
}

module.exports = {
    sendRecoveryCode,
    sendReport,
    sendNewOrderNotification,
    sendOrderIsCancelledNotification,
    sendSuborderIsCancelledNotification,
    sendNewSuborderStatus
};
