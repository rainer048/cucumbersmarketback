const moment = require('moment');

const SET_ORDER_STATUS_FROM_TIME = 12;
const SET_ORDER_STATUS_TO_TIME = 14;

const checkSetOrderStatusTime = async (ctx, next) => {
    if (moment().unix() < moment().startOf('day').add(SET_ORDER_STATUS_FROM_TIME, 'hours').unix() ||
        moment().unix() > moment().startOf('day').add(SET_ORDER_STATUS_TO_TIME, 'hours').unix()) {

        let body = `{ status: 'error', message: 'Статус заказа можно изменять только с 12.00 до 14.00' }`;
        ctx.throw(400, body, {headers: { 'Access-Control-Allow-Origin': '*'} });
        return;
    }
    return next();
};

module.exports = {
    checkSetOrderStatusTime
};
