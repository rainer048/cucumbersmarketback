const PdfPrinter = require('pdfmake');
const ordersQueries = require('../db/queries/orders');
const moment = require('moment');


const printer = new PdfPrinter({
    Roboto: {
        normal: 'src/server/helpers/fonts/Roboto_Regular.ttf',
        bold: 'src/server/helpers/fonts/Roboto_Bold.ttf',
        italics: 'src/server/helpers/fonts/Roboto_Italic.ttf',
        bolditalics: 'src/server/helpers/fonts/Roboto_BoldItalic.ttf'
    }
});

let myTableLayouts = {
    myLayout: {
        hLineWidth: function (i, node) {
            return (i === node.table.headerRows || i === node.table.headerRows-1) ? 2 : 1;
        },
        vLineWidth: function (i) {
            return 1;
        },
        hLineColor: function (i) {
            return i === 1 ? 'black' : '#aaa';
        },
        paddingLeft: function (i) {
            return i === 0 ? 0 : 8;
        },
        paddingRight: function (i, node) {
            return (i === node.table.widths.length - 1) ? 0 : 8;
        },
    }
};

async function getReport(marketId, fromTime, toTime, orderBy, order, type) {

    let fromTimeStr = moment.unix(fromTime).utcOffset(3*60).locale('ru').format('MMMM DD, YYYY год, HH:mm');
    let toTimeStr = moment.unix(toTime).utcOffset(3*60).locale('ru').format('MMMM DD, YYYY год, HH:mm');
    let creationTime = moment().utcOffset(3*60).locale('ru').format('dddd, MMMM DD, YYYY год, HH:mm');
    let [ fromDB, market, total ] = await ordersQueries.getMarketSubordersForReport(marketId, fromTime, toTime, 0, 0, orderBy, order, type);
    let data = [ ['№', 'Дата и время заявки', 'Заказчик', 'Телефон заказчика', 'Категория', 'Продукт', 'Сорт', 'Количество, кг', 'Цена, руб', 'Статус']];

    fromDB.forEach((element) => {
        delete element.status_id;
        element.created_at = moment.unix(element.created_at).utcOffset(3*60).locale('ru').format('MMMM DD, YYYY г, HH:mm');
        data.push(Object.values(element))
    });

    let docDefinition = {
        pageOrientation: 'landscape',
        content: [
            {
                image: './src/server/helpers/fm_for_docs.jpg',
                alignment: 'left',
                width: 100
            },
            {
                text: 'Отчёт',
                fontSize: 40,
                alignment: 'center'
            },
            {
                text: `о заявках по фермерскому хозяйству "${market}"\nза период: ${fromTimeStr} - ${toTimeStr}`,
                fontSize: 16,
                alignment: 'center'
            },
            {
                layout: 'myLayout', // optional
                table: {
                    headerRows: 1,
                    widths: [ 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'],
                    body: data

                },
                margin: [0, 30],
                alignment: 'center',
                fontSize: 11
            },
        ],
        footer: (currentPage, pageCount) => {
            return {
                columns: [
                    {
                        alignment: 'left',
                        text: 'farmer-market.ru',
                        fontSize: 10
                    },
                    {
                        alignment: 'right',
                        text: `Дата формирования отчета: ${creationTime}\nЛист ${currentPage} из ${pageCount}`,
                        fontSize: 10
                    }
                ],
                margin: [40, 0]
            }
        },
        header: {
            columns: [
                {
                    text: `Фермерское хозяйство: "${market}"\nПериод: ${fromTimeStr} - ${toTimeStr}`,
                    alignment: 'right',
                    fontSize: 10,
                    margin: [0, 10, 40, 0]
                }
            ]

        }
    };

    let pdfDoc = printer.createPdfKitDocument(docDefinition, {tableLayouts: myTableLayouts});

    let pdfData = 'data:application/pdf;base64,' + (await getBase64(pdfDoc)).toString('base64');

    return {
        pdfData: pdfData,
        market: market,
        fromTime: fromTimeStr,
        toTime: toTimeStr,
        creationTime: creationTime,
        total: total
    };
}

async function getBase64(obj) {
    return new Promise((resolve, reject) => {
        try {
            let chunks = [];
            let result;
            obj.on('readable', () => {
                let chunk;
                while ((chunk = obj.read(obj.bufferSize)) !== null) {
                    chunks.push(chunk);
                }
            });
            obj.on('end', () => {
                result = Buffer.concat(chunks).toString('base64');
                resolve(result);
            });
            obj.end();
        } catch (e) {
            reject(e);
        }
    });
}

module.exports = {
    getReport
};
