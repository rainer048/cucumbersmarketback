const authQueries = require('../db/queries/auth');
const usersQueries = require('../db/queries/users');
const marketsQueries = require('../db/queries/markets');
const jwt = require('jsonwebtoken');
const hash = require('object-hash');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const jwtsecret = 'very very secret key';

const ACCESS_TOKEN_TIMEOUT = '1h';
const REFRESH_TOKEN_TIMEOUT = '24h';

function getCookiesConfig() {
    return {
        access_token: {
            httpOnly: false,
            expires: new Date(Date.now() + 1000 * 60 * 60 * 1)              // expires in 1 hour
        },
        refresh_token: {
            httpOnly: false,
            expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 1)         // expires in 24 hours
        }
    }
}

async function verifyToken(token) {
    if (!token) return false;
    return jwt.verify(token, jwtsecret, async (err, res) => {
        return !!res;
    });
}


async function updateTokens(id) {
    const payload = {id: id};
    const access_token = jwt.sign(payload, jwtsecret, {expiresIn: ACCESS_TOKEN_TIMEOUT}); //здесь создается JWT
    const refresh_token = jwt.sign(payload, jwtsecret, {expiresIn: REFRESH_TOKEN_TIMEOUT}); //здесь создается JWT
    await authQueries.setRefreshToken(id, refresh_token);
    // await usersQueries.updateLastActivityTime(id);
    return {access_token: access_token, refresh_token: refresh_token};
}

function decodeToken(token) {
    return jwt.decode(token, jwtsecret);
}

function encodeData(text, secret) {
    return crypto.createHmac('sha1', secret).update(text).digest('hex');
}

function makeSecret(data) {
    return hash(data);
}



let checkAuthorization = async (ctx, next) => {

    let accessToken = ctx.cookies.get('access_token');
    let refreshToken = ctx.cookies.get('refresh_token');
    if (refreshToken && await verifyToken(refreshToken)) {
        let id;
        if (accessToken && await verifyToken(accessToken)) {
            id = (await jwt.decode(accessToken, jwtsecret)).id;
        } else {
            id = (await jwt.decode(refreshToken, jwtsecret)).id;
            if ((await authQueries.getRefreshToken(id)) === refreshToken) {
                let tokens = await updateTokens(id);
                let cookiesConfig = getCookiesConfig();
                ctx.cookies.set('access_token', tokens.access_token, cookiesConfig.access_token);
                ctx.cookies.set('refresh_token', tokens.refresh_token, cookiesConfig.refresh_token);
            } else {
                let body = `{ status: 'error', message: 'Пользователь не авторизован!'}`;
                ctx.throw(401, body, {headers: { 'Access-Control-Allow-Origin': '*'} });
                return;
            }
        }
        ctx.state.user = await usersQueries.getUser({id: id});
        ctx.state.loggedIn = true;
        return next();
    } else {
        let body = `{ status: 'error', message: 'Пользователь не авторизован!'}`;
        ctx.throw(401, body, {headers: { 'Access-Control-Allow-Origin': '*'} });
        return;
    }
};

let checkLoggedIn = async (ctx, next) => {

    let accessToken = ctx.cookies.get('access_token');
    let refreshToken = ctx.cookies.get('refresh_token');
    let id = null;
    let loggedIn = false;

    if (refreshToken && await verifyToken(refreshToken) && (await authQueries.getRefreshToken((await jwt.decode(refreshToken, jwtsecret)).id)) === refreshToken) {
        if (accessToken && await verifyToken(accessToken)) {
            id = (await jwt.decode(accessToken, jwtsecret)).id;
            loggedIn = true;
        } else {
            id = (await jwt.decode(refreshToken, jwtsecret)).id;
            let tokens = await updateTokens(id);
            let cookiesConfig = getCookiesConfig();
            ctx.cookies.set('access_token', tokens.access_token, cookiesConfig.access_token);
            ctx.cookies.set('refresh_token', tokens.refresh_token, cookiesConfig.refresh_token);
            loggedIn = true;
        }
    }

    let user = id ? await usersQueries.getUser({id: id}) : null;

    if (user) {
        if (user.isFarmer) {
            user.whereOwner = await marketsQueries.getWhereOwner(user.id);
        }
        if (user.isManager) {
            user.whereManager = await marketsQueries.getWhereManager(user.id);
        }
        if (user.isCourier) {
            user.whereCourier = await marketsQueries.getWhereCourier(user.id);
        }
    }

    ctx.state.user = user;
    ctx.state.loggedIn = loggedIn;
    return next();
};

async function getPasswordHash(plaintextPassword) {
    return await bcrypt.hashSync(plaintextPassword, saltRounds, function (err, hash) {
        return hash;
    });
}

async function comparePassword(plaintextPassword, hash) {
    return await bcrypt.compareSync(plaintextPassword, hash, function(err, res) {
        return res;
    });
}

module.exports = {
    updateTokens,
    verifyToken,
    decodeToken,
    checkAuthorization,
    checkLoggedIn,
    encodeData,
    makeSecret,
    getCookiesConfig,
    getPasswordHash,
    comparePassword
};
