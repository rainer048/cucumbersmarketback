const knex = require('../connection');

function getUser(data) {
    return knex('users')
        .where(data)
        .first()
        .select('id','email','telephone','first_name','second_name',
            'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
            'address', 'rating', 'isAdmin', 'isFarmer', 'becomeFarmer', 'isManager', 'subscription', 'created_at', 'updated_at');
}

async function getAllUsers(limit, offset, orderBy, order) {

    let total = await knex('users')
        .count('id')
        .first()
        .then( res => res.count);

    let users = await knex('users')
        .select('id','email','telephone','first_name','second_name',
            'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
            'address', 'rating', 'isAdmin', 'isFarmer', 'becomeFarmer', 'isManager', 'subscription', 'created_at', 'updated_at')
        .limit(limit)
        .offset(offset)
        .orderBy(orderBy, order);

    return {total: total, users: users};
}

function editProfile(id, data) {
    return knex('users')
        .where({
            id: id
        })
        .first()
        .update(data)
        .returning(['id','email','telephone','first_name','second_name',
            'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
            'address', 'rating', 'isAdmin', 'isFarmer', 'becomeFarmer', 'isManager', 'subscription', 'created_at', 'updated_at'])
        .then( (res) => res[0]);
}

function setPassword(id, hash) {

    return knex('users')
        .where({
            id: id
        })
        .update({
            password: hash
        });
}


async function findUsers(searchQuery) {

    return await knex('users')
        .select('*')
        .whereRaw(`to_tsvector('russian', first_name) || to_tsvector('russian', second_name) || 
            to_tsvector('russian', email) || to_tsvector('russian', telephone) @@ plainto_tsquery('russian', '${searchQuery}') `)
        .limit(15);

}

async function setBecomeFarmer(userId, becomeFarmer) {

    let data = becomeFarmer ? {
        becomeFarmer: true
    } : {
        becomeFarmer: false,
        isFarmer: false
    };

    if (!becomeFarmer) {
        await knex('markets')
            .update({
                active: false
            })
            .where({
                owner: userId
            });
    }

    return await knex('users')
        .update(data)
        .where({
            id: userId
        })
        .returning(['id','email','telephone','first_name','second_name',
            'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
            'address', 'rating', 'isAdmin', 'isFarmer', 'becomeFarmer', 'isManager', 'subscription', 'created_at', 'updated_at'])
        .then( (res) => res[0]);

}

async function getFarmers(limit, offset, orderBy, order) {

    let total = await knex('users')
        .where({
            isFarmer: true
        })
        .count('id')
        .first()
        .then( res => res.count);

    let users = await knex('users')
        .where({
            isFarmer: true
        })
        .select('id','email','telephone','first_name','second_name',
            'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
            'address', 'rating', 'isAdmin', 'isFarmer', 'becomeFarmer', 'isManager', 'subscription', 'created_at', 'updated_at')
        .limit(limit)
        .offset(offset)
        .orderBy(orderBy, order);

    return {total: total, users: users};
}

async function getFarmerRequests(limit, offset, orderBy, order) {

    let total = await knex('users')
        .where({
            isFarmer: false,
            becomeFarmer: true
        })
        .count('id')
        .first()
        .then( res => res.count);

    let users = await knex('users')
        .where({
            isFarmer: false,
            becomeFarmer: true
        })
        .select('id','email','telephone','first_name','second_name',
            'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
            'address', 'rating', 'isAdmin', 'isFarmer', 'becomeFarmer', 'isManager', 'subscription', 'created_at', 'updated_at')
        .limit(limit)
        .offset(offset)
        .orderBy(orderBy, order);

    return {total: total, users: users};
}

async function setIsFarmer(userId, isFarmer) {

    return await knex('users')
        .update({
            isFarmer: isFarmer
        })
        .where({
            id: userId
        })
        .returning(['id','email','telephone','first_name','second_name',
            'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
            'address', 'rating', 'isAdmin', 'isFarmer', 'becomeFarmer', 'isManager', 'subscription', 'created_at', 'updated_at'])
        .then( (res) => res[0]);;

}

module.exports = {
    getUser,
    editProfile,
    setPassword,
    findUsers,
    getAllUsers,
    setBecomeFarmer,
    getFarmers,
    getFarmerRequests,
    setIsFarmer
};
