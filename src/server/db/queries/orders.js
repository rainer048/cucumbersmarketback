const knex = require('../connection');
const moment = require('moment');

async function getOrder(id) {
    return knex('orders')
        .joinRaw('inner join order_statuses on order_statuses.id = orders.status')
        .where({
            'orders.id': id
        })
        .select(['orders.id', 'customer', 'suborders', 'sum', 'delivery_type', 'delivery_address', 'type', 'pay_type',
            'order_statuses.status', 'order_statuses.status_description', 'created_at', 'updated_at'])
        .first()
        .then( async (order) => {
            if (order) {
                order.suborders = await knex('suborders')
                    .joinRaw('inner join order_statuses on order_statuses.id = suborders.status')
                    .whereIn('suborders.id', order.suborders)
                    .select(['suborders.id', 'product', 'count', 'cost', 'order_statuses.status', 'order_statuses.status_description', 'created_at', 'updated_at']);
                for await (suborder of order.suborders) {
                    suborder.product = await knex('products')
                        .where({
                            'id': suborder.product
                        })
                        .first()
                        .select('*');
                }
            }
            return order;
        });
}

async function getSuborder(id) {

    return knex('suborders')
        .joinRaw('inner join order_statuses on order_statuses.id = suborders.status')
        .where({
            'suborders.id': id
        })
        .select('*')
        .first();
}


async function getMarketSuborders(marketId, fromTime, toTime, limit, offset, orderBy, order, type) {

    console.log(type);

    let total =  await knex('suborders')
        .joinRaw(`inner join orders on suborders.id = any (orders.suborders) inner join products on products.id = suborders.product 
            inner join order_statuses on order_statuses.id = suborders.status`)
        .whereRaw( `market = ${marketId} and orders.type = '${type}' and suborders.created_at >= ${fromTime} and suborders.created_at <= ${toTime}`)
        .count('suborders.id')
        .first()
        .then( res => res.count);

    let suborders =  await knex('suborders ')
        .select(knex.raw( `suborders.id, customer, product, products.name as product_name,
            market, count, cost, suborders.status as status_id, order_statuses.status, shipping_date, suborders.created_at, suborders.updated_at`))
        .joinRaw(`inner join orders on suborders.id = any (orders.suborders) inner join products on products.id = suborders.product 
            inner join order_statuses on order_statuses.id = suborders.status`)
        .whereRaw( `market = ${marketId} and orders.type = '${type}' and suborders.created_at >= ${fromTime} and suborders.created_at <= ${toTime}`)
        .limit(limit)
        .offset(offset)
        .orderBy(orderBy, order);

    return {
        suborders: suborders,
        total: total
    };
}

async function getMarketSubordersForReport(marketId, fromTime, toTime, limit, offset, orderBy, order, type) {

    let total = await knex('suborders')
        .joinRaw(`inner join orders on suborders.id = any (orders.suborders) 
            inner join products on products.id = suborders.product  
            inner join users on users.id = orders.customer 
            inner join product_categories on product_categories.id = products.category
            inner join order_statuses on order_statuses.id = suborders.status`)
        .whereRaw( `market = ${marketId} and orders.type = '${type}' and suborders.created_at >= ${fromTime} and suborders.created_at <= ${toTime}`)
        .count('suborders.id')
        .first()
        .then(res => res.count);

    let data = await knex('suborders')
        .select(knex.raw( `suborders.id, suborders.created_at, users.first_name || ' ' || users.second_name as customer, 
            users.telephone, product_categories.name as category, products.name as product_name, products.sort, count, cost, suborders.status as status_id, order_statuses.status`))
        .joinRaw(`inner join orders on suborders.id = any (orders.suborders) 
            inner join products on products.id = suborders.product 
            inner join users on users.id = orders.customer 
            inner join product_categories on product_categories.id = products.category
            inner join order_statuses on order_statuses.id = suborders.status`)
        .whereRaw( `market = ${marketId} and orders.type = '${type}' and suborders.created_at >= ${fromTime} and suborders.created_at <= ${toTime}`)
        .orderBy(orderBy, order)
        .limit(limit || null)
        .offset(offset || 0);

    let market = await knex('markets')
        .select('name')
        .where({
            id: marketId
        })
        .first()
        .then(res => res.name);

    return [data, market, total];
}

async function addOrder(userId, suborders, type, pay_type, delivery_type, delivery_address) {

    let res = await knex.transaction(async (trx) => {
        let subordersIds = [];
        let i = 1;

        let orderId = await knex('orders')
            .insert({
                customer: userId,
                type: type,
                pay_type: pay_type,
                delivery_type: delivery_type,
                delivery_address: delivery_address
            }, 'id')
            .transacting(trx);

        let sum = 0;

        for await (const suborder of suborders) {

            sum += suborder.count * suborder.cost;

            suborder.id = orderId.toString() + '-' + i++;

            let suborderId = await knex('suborders')
                .insert(suborder, 'id')
                .transacting(trx);
            subordersIds.push(suborderId.toString());
        }

        await knex('orders')
            .where({
                id: orderId[0]
            })
            .update({
                suborders: subordersIds,
                sum: sum
            })
            .transacting(trx);

        return orderId;
    });

    return await getOrder(res[0]);
}


function editOrder(id, data) {
    return knex('orders')
        .where({
            id: id
        })
        .update(data)
        .returning('*');
}

async function getOrderBySuborder(id) {

    //console.log(id);

    return await knex('orders')
        .select('*')
        .first()
        .whereRaw(`'${id}' = ANY (suborders)`);
}

async function getOrders(customer, limit, offset, status, type) {

    let whereObj = {
        customer: customer
    };

    if (type) whereObj.type = type;

    let whereStr = status ? `${status} = any(select status from suborders where suborders.id = any(orders.suborders))` : '';

    let total = await knex('orders')
        .where(whereObj)
        .whereRaw(whereStr)
        .count('id')
        .first()
        .then( res => res.count);

    let orders = await knex('orders')
        .joinRaw('inner join order_statuses on order_statuses.id = orders.status')
        .where(whereObj)
        .whereRaw(whereStr)
        .select(['orders.id', 'customer', 'sum', 'suborders',  'delivery_type', 'delivery_address', 'type', 'pay_type',
            'order_statuses.status', 'order_statuses.status_description', 'created_at', 'updated_at'])
        .orderBy('id', 'desc')
        .limit(limit)
        .offset(offset)
        .then( async (orders) => {
            for await (order of orders) {
                order.suborders = await knex('suborders')
                    .joinRaw('inner join order_statuses on order_statuses.id = suborders.status')
                    .whereIn('suborders.id', order.suborders)
                    .select(['suborders.id', 'product', 'count', 'cost', 'order_statuses.id as status_id', 'order_statuses.status', 'order_statuses.status_description', 'created_at', 'updated_at']);
                for await (suborder of order.suborders) {
                    suborder.product = await knex('products')
                        .where({
                            'id': suborder.product
                        })
                        .first()
                        .select('*');
                }
            }
            return orders;
        });


    if (status) {

        orders.forEach((order) => {
            order.suborders = order.suborders.filter((suborder) => {
                return suborder.status_id === status;
            })
        });
    }


    return {
        orders: orders,
        total: total
    }
}

async function cancelSuborder(id) {
    return await knex('suborders')
        .where({
            id: id,
            status: 1
        })
        .update({
            status: 5
        })
        .returning('*');
}


async function cancelOrder(id) {
    return await knex('orders')
        .where({
            id: id
        })
        .select('suborders')
        .first()
        .then(async res => {
            await knex('suborders')
                .whereIn('id', res.suborders)
                .andWhere({
                    status: 1
                })
                .update({
                    status: 5
                })
                .returning('*');
        });
}

async function setSuborderStatus(id, status) {
    return await knex('suborders')
        .where({
            id: id
        })
        .update({
            status: status
        })
        .returning('*');
}


async function isSuborderCustomer(suborder, user) {

    let order = await getOrderBySuborder(suborder);
    return order.customer === user;
}

async function isOrderCustomer(order, user) {

    let customer = await knex('orders')
        .where({
            id: order
        })
        .first()
        .then(res => res.customer);
    return customer === user;
}

async function isTodaySuborder(suborder) {

    let created_at = await knex('suborders')
        .where({
            id: suborder
        })
        .select('created_at')
        .first()
        .then(res => res.created_at);

    return created_at >= moment().startOf('day').subtract(12, 'hours').unix() &&
        created_at <= moment().startOf('day').add(12, 'hours').unix();
}


async function getStatusList() {

    return await knex('suborder_statuses')
        .select('*');

}


module.exports = {
    getOrder,
    getSuborder,
    addOrder,
    editOrder,
    getMarketSuborders,
    getOrders,
    getOrderBySuborder,
    getMarketSubordersForReport,
    cancelOrder,
    cancelSuborder,
    isSuborderCustomer,
    isOrderCustomer,
    setSuborderStatus,
    isTodaySuborder,
    getStatusList
};
