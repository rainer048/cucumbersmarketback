const knex = require('../connection');

async function addMessage(data) {
    return await knex('feedback')
        .insert(data)
        .returning('*')
        .then(res => res[0]);
}

async function getMessages(limit, offset) {

    let total = await knex('feedback')
        .count('id')
        .first()
        .then( res => res.count);

    let messages = await knex('feedback')
        .select('*')
        .orderBy('id', 'desc')
        .limit(limit)
        .offset(offset);
    return {
        messages: messages,
        total:  total
    }
}

async function getMessage(id) {

    return await knex('feedback')
        .select('*')
        .where({
            id: id
        })
        .first();
}


async function deleteMessage(id) {

    return await knex('feedback')
        .where({
            id: id
        })
        .del();

}

module.exports = {
    addMessage,
    getMessage,
    getMessages,
    deleteMessage
};
