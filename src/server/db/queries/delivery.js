const knex = require('../connection');

async function addAddress(data) {
    return await knex('delivery_addresses')
        .insert(data)
        .returning('*')
        .then(res => res[0]);
}

async function getMyAddresses(userId) {

    return await knex('delivery_addresses')
        .select('*')
        .where({
            owner: userId
        });

}

async function editAddress(id, data) {

    return await knex('delivery_addresses')
        .where({
            id: id
        })
        .update(data)
        .returning('*')
        .then(res => res[0]);
}


async function deleteAddress(id) {

    return await knex('delivery_addresses')
        .where({
            id: id
        })
        .del();

}

module.exports = {
    addAddress,
    getMyAddresses,
    editAddress,
    deleteAddress
};
