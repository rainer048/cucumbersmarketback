const knex = require('../connection');

async function getInfo(topic) {
    return knex('info')
        .where({
            topic: topic
        })
        .select('text')
        .first();
}

async function getAllTopics() {

    return await knex('info')
        .select(['topic', 'name']);
}

async function addInfo(info) {

    return knex('info')
        .insert(info)
        .returning('*')
        .then(res => res[0]);
}


function editInfo(topic, data) {
    return knex('info')
        .where({
            topic: topic
        })
        .update(data)
        .returning('*')
        .then( res => res[0]);
}

function deleteInfo(topic) {
    return knex('info')
        .where({
            topic: topic
        })
        .del();
}
module.exports = {
    getInfo,
    addInfo,
    editInfo,
    deleteInfo,
    getAllTopics
};
