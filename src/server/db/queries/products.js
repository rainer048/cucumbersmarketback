const knex = require('../connection');
const usersQueries = require('./users');

async function getOneById(userId, id) {

    try {
        let product = await knex('products')
            .whereRaw(`id = ${id} and ((active = true and true = (select active from markets where id = 
                products.market)) or ${userId} = (select owner from markets where id = products.market) 
                or true = (select "isAdmin" from users where id = ${userId}) or true = (select "canEditProducts" 
                from managers where manager = ${userId} and active = true and market = products.market))`)
            .andWhere({
                id: id
            })
            .select('*')
            .first();

        if (product) {
            let market = await knex('markets')
                .where({
                    id: product.market
                })
                .select('*')
                .first();

            product.market = market;
            product.isOwner = userId === market.owner;

        }


        let canEdit = ((await knex('markets')
                .select('owner')
                .where({
                    id: product.market.id
                })
                .first()).owner === userId) ||
            (await knex('managers')
                .select(`canEditProducts`)
                .where({
                    market: product.market.id,
                    manager: userId,
                    active: true
                })
                .first()
                .then( res => {
                    return res ? res.canEditProducts : false
                }));

        return {
            product: product,
            canEdit: canEdit
        };
    } catch (err) {
        console.log(err);
        return null;
    }
}

async function getProductImage(id) {
    return knex('products')
        .where({
            id: id
        })
        .select('image')
        .first()
        .then((res) => {
            return res.image;
        });
}

async function getMany(limit, offset, orderBy, order, isAdmin, type) {

    let whereObj = {};
    if (!isAdmin) whereObj.active = true;
    if (type) whereObj[type] = true;

    let orderArr = [{column: orderBy, order: order}, {column: 'sort', order: 'asc'}, {column: 'id', order: 'asc'}];

    let total = await knex('products')
        .where(whereObj)
        .whereRaw('true = (select active from markets where id = products.market)')
        .count('id')
        .first()
        .then( res => res.count);

    let products = await knex('products')
        .select('*')
        .where(whereObj)
        .whereRaw('true = (select active from markets where id = products.market)')
        .orderBy(orderArr)
        .limit(limit)
        .offset(offset);

    for await (let product of products) {
        product.market = await knex('markets')
            .where({
                id: product.market
            })
            .select('*')
            .first();

    }


    return {
        products: products,
        total: total
    };

}

async function getByParameters(limit, offset, orderBy, order, category, market, isAdmin, type) {

    let whereObj = {};
    if (category) whereObj.category = category;
    if (market) whereObj.market = market;
    if (!isAdmin) whereObj.active = true;
    if (type) whereObj[type] = true;

    let orderArr = [{column: orderBy, order: order}, {column: 'sort', order: 'asc'}, {column: 'id', order: 'asc'}];

    let total = await knex('products')
        .count('id')
        .where(whereObj)
        .whereRaw('true = (select active from markets where id = products.market)')
        .first()
        .then( res => res.count);

    let products = await knex('products')
        .select('*')
        .where(whereObj)
        .whereRaw('true = (select active from markets where id = products.market)')
        .orderBy(orderArr)
        .limit(limit)
        .offset(offset);

    for await (let product of products) {
        product.market = await knex('markets')
            .where({
                id: product.market
            })
            .select('*')
            .first();

    }

    return {
        products: products,
        total:  total
    };

}

async function getByMarketId(userId, id, limit, offset, orderBy, order) {

    let total = await knex('products')
        .whereRaw(`((active = true and true = (select active from markets where id = products.market)) or ${userId} = 
            (select owner from markets where id = products.market) or true = (select "isAdmin" from users where id = ${userId}))`)
        .andWhere({
            market: id
        })
        .count('id')
        .first()
        .then( res => res.count);

    let products = await knex('products')
        .select('*')
        .whereRaw(`((active = true and true = (select active from markets where id = products.market))
            or ${userId} = (select owner from markets where id = products.market) or true = (select "isAdmin" from users where id = ${userId}))`)
        .andWhere({
            market: id
        })
        .orderBy(orderBy, order)
        .limit(limit)
        .offset(offset);

    for await (let product of products) {
        let market = await knex('markets')
            .where({
                id: product.market
            })
            .select('*')
            .first();

        product.market = market;

    }

    return {
        products: products,
        total: total
    };

}

async function getByMyMarkets(userId, limit, offset, orderBy, order, market, onlyActive) {

    let user = await usersQueries.getUser({id: userId});

    let marketIds = [market];

    if (!market) {
        let ownedMarketsIds = user.isFarmer ? (await getMarketIdsArray(userId)) : [];
        let managedMarketsIds = await getManagedMarkets(userId);
        marketIds = [ ...new Set([...ownedMarketsIds,...managedMarketsIds])];
    }

    let whereActive = onlyActive ? {'products.active': true} : {'markets.active': true};

    let total = await knex('products')
        .innerJoin('markets', 'markets.id', 'products.market')
        .whereIn('markets.id', marketIds)
        .where(whereActive)
        .count('products.id')
        .first()
        .then( res => res.count);

    let products = await knex('products')
        .innerJoin('markets', 'markets.id', 'products.market')
        .select(['products.id', 'products.name', 'products.category', 'products.sort',
            'products.description', 'products.limit', 'products.image', 'products.market', 'products.total_sold',
            'products.retail', 'products.wholesale', 'products.retail_price',
            'products.active', 'products.created_at' , 'products.updated_at', 'markets.name as marketName'])
        .whereIn('products.market', marketIds)
        .where(whereActive)
        .orderBy(orderBy, order)
        .limit(limit)
        .offset(offset);

    for await (let product of products) {
        delete product.marketName;
        product.market = await knex('markets')
            .where({
                id: product.market
            })
            .select('*')
            .first();

    }

    return {
        products: products,
        total: total
    };

}

async function getTop(type) {

    let where = {
        active: true
    };

    if (type) where[type] = true;

    let products = await knex('products')
        .select('*')
        .where(where)
        .whereRaw('true = (select active from markets where id = products.market)')
        .orderBy('total_sold', 'desc')
        .limit(10);

    for await (let product of products) {
        product.market = await knex('markets')
            .where({
                id: product.market
            })
            .select('*')
            .first();

    }

    return products;

}

async function getNew(type) {

    let where = {
        active: true
    };

    if (type) where[type] = true;

    let products = await knex('products')
        .select('*')
        .where(where)
        .whereRaw('true = (select active from markets where id = products.market)')
        .orderBy('created_at', 'desc')
        .limit(10);

    for await (let product of products) {
        product.market = await knex('markets')
            .where({
                id: product.market
            })
            .select('*')
            .first();

    }

    return products;

}

async function find(searchQuery, limit, offset, orderBy, order, type) {

    let total = await knex('products')
        .whereRaw(`to_tsvector('russian', description) || to_tsvector('russian', name) @@ plainto_tsquery('russian', '${searchQuery}') 
            and products.${type} = true and active = true and true = (select active from markets where id = products.market)`)
        .count('id')
        .first()
        .then( res => res.count);


    let products = await knex('products')
        .select('*')
        .whereRaw(`to_tsvector('russian', description) || to_tsvector('russian', name) @@ plainto_tsquery('russian', '${searchQuery}') and products.${type} = true 
        and active = true and true = (select active from markets where id = products.market)`)
        .orderBy(orderBy, order)
        .limit(limit)
        .offset(offset);

    for await (let product of products) {
        product.market = await knex('markets')
            .where({
                id: product.market
            })
            .select('*')
            .first();

    }

    return {
        total: total,
        products: products
    };

}

function addProduct(product) {

    return knex('products')
        .insert(product)
        .returning('*')
        .then(res => res[0]);

}

function editProduct(id, data) {
    return knex('products')
        .where({
            id: id
        })
        .update(data)
        .returning('*')
        .then(res => res[0]);
}

async function getMarketIdsArray(ownerId, onlyActive) {

    let whereObj = {
        owner: ownerId,
        active: true
    };

    return knex('markets')
        .select('id')
        .where(whereObj)
        .then( res => {
            let arr = [];
            res.forEach( item => {
                arr.push(item.id)
            });
            return arr;
        });
}

async function getManagedMarkets(userId) {
    return knex('managers')
        .innerJoin('markets', 'managers.market', 'markets.id')
        .select('market')
        .where({
            manager: userId,
            'managers.active': true,
            canEditProducts: true,
            'markets.active': true
        })
        .then( res => {
            let arr = [];
            res.forEach( item => {
                arr.push(item.market)
            });
            return arr;
        });
}

async function getManyByIds(ids) {

    let products = await knex('products')
        .select('*')
        .whereIn('id', ids);

    for await (let product of products) {
        if (!product.image) product.image = 'img/noimage.jpg';
        product.market = await knex('markets')
            .where({
                id: product.market
            })
            .select('*')
            .first();
    }

    let res = products.reduce( (result, curr) => {
        result[curr.id] = curr;
        return result;
    }, {});

    return res;

}


module.exports = {
    getOneById,
    getMany,
    getByParameters,
    getByMarketId,
    getByMyMarkets,
    getTop,
    getNew,
    find,
    addProduct,
    editProduct,
    getProductImage,
    getManagedMarkets,
    getManyByIds
};
