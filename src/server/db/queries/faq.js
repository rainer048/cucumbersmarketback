const knex = require('../connection');

async function getFaq() {


    let topics = await knex('faq_topics')
        .select('*');

    for await (let topic of topics) {
       let faq = await knex('faq')
           .select('*')
           .where({
               topic: topic.id
           })
           .orderBy('id', 'asc');
       topic.faq = faq;
    }

    return topics;
}

async function getFaqByTopic(topic) {

    return await knex('faq')
        .where({
            topic: topic
        })
        .select('*');
}

async function getFaqById(id) {

    return await knex('faq')
        .where({
            id: id
        })
        .select('*')
        .first();
}

async function editFaq(id, data) {

    return await knex('faq')
        .where({
            id: id
        })
        .update(data)
        .returning('*')
        .then(res => res[0]);

}

async function editFaqTopic(id, topic) {

    return await knex('faq_topics')
        .where({
            id: id
        })
        .update({
            topic: topic
        })
        .returning('*')
        .then(res => res[0]);

}

async function addFaqTopic(topic) {

    return await knex('faq_topics')
        .insert({
            topic: topic
        })
        .returning('*')
        .then(res => res[0]);

}

async function addFaq(faq) {

    return await knex('faq')
        .insert(faq)
        .returning('*')
        .then(res => res[0]);

}

async function deleteFaq(id) {

    return await knex('faq')
        .where({
            id: id
        })
        .del();

}

async function deleteFaqTopic(id) {

    return await knex('faq_topics')
        .where({
            id: id
        })
        .del();

}

async function getFaqTopics() {

    return await knex('faq_topics')
        .select('*')
        .orderBy('id', 'asc');
}

module.exports = {
    getFaq,
    getFaqById,
    editFaq,
    addFaq,
    deleteFaq,
    getFaqByTopic,
    editFaqTopic,
    addFaqTopic,
    deleteFaqTopic,
    getFaqTopics
};
