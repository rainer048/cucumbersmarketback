const knex = require('../connection');

async function userRegistration(email, telephone, password, first_name, second_name, subscription) {

    try {
        return await knex('users')
            .insert({
                email: email,
                telephone: telephone,
                password: password,
                first_name: first_name,
                second_name: second_name,
                subscription: subscription
            })
            .returning(['id', 'email', 'telephone', 'first_name', 'second_name',
                'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
                'address', 'isAdmin', 'isFarmer', 'created_at', 'updated_at'])
            .then(res => res[0]);
    } catch (err) {
        //console.log(err);
        throw(err);
    }
};

module.exports = {
    userRegistration
};
