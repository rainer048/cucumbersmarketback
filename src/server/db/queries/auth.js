const knex = require('../connection');

async function getRefreshToken(id) {
    let res = await knex('users')
        .where({
            id: id
        })
        .first()
        .select('refresh_token');
    return res.refresh_token;
};

async function setRefreshToken(id, token) {
    await knex('users')
        .where({
            id: id
        })
        .first()
        .update(
            {
                refresh_token: token
            }
        )
};

async function destroyTokens(id) {
    await knex('users')
        .where({
            id: id
        })
        .first()
        .update(
            {
                refresh_token: ''
            }
        );
    return true;
};

async function getHash(id) {
    return await knex('users')
        .where({id: id})
        .first()
        .select('password')
        .then( (res) => {
            return res.password;
        });
}

module.exports = {
    getRefreshToken,
    setRefreshToken,
    destroyTokens,
    getHash
};
