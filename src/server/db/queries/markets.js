const knex = require('../connection');

async function getMany(limit, offset, admin) {

    let whereObj = {};
    if (!admin) whereObj.active = true;

    let total = await knex('markets')
        .where(whereObj)
        .count('id')
        .first()
        .then( res => res.count);

    let markets = await knex('markets')
        .where(whereObj)
        .select('*')
        .orderBy('id', 'desc')
        .limit(limit)
        .offset(offset);

    return {
        markets: markets,
        total: total
    }
}


async function getOne(id) {
    return knex('markets')
        .where({
            id: id
        })
        .select('*')
        .first();
}


async function add(id, data) {
    data.owner = id;
    let market = await knex('markets')
        .insert(data)
        .returning('*')
        .then( res => res[0]);

    await addManager(market.id, id, true, true);

    return market;

}

function edit(id, data) {
    return knex('markets')
        .where({
            id: id
        })
        .update(data)
        .returning('*')
        .then( res => res[0]);
}

async function isOwner(market, user) {

    try {
        let owner = await knex('markets')
            .select('owner')
            .where({
                id: market
            })
            .first()
            .then((res) => res.owner);
        return owner === user;
    } catch {
        return false;
    }
}

async function isManager(market, user) {


    let res = await knex('managers')
        .select('active')
        .where({
            market: market,
            manager: user
        })
        .first()
        .then(res => {
            return res ? res.active : false;
        });

    return res;
}

async function isManagerBySuborder(suborder, user) {

    let market = await knex('products')
        .select('market')
        .whereRaw(`id = (select product from suborders where id = '${suborder}')`)
        .first()
        .then(res => {
            return res ? res.market : null;
        });

    return market ? await isManager(market, user) : false;


}

async function isOwnerBySuborder(suborder, user) {

    let market = await knex('products')
        .select('market')
        .whereRaw(`id = (select product from suborders where id = '${suborder}')`)
        .first()
        .then(res => {
            return res ? res.market : null;
        });

    return market ? market.owner === user : false;


}

async function addManager(market, manager, notifications, canEditProducts) {
    return await knex
        .raw(`insert into managers (manager, market, active, notifications, "canEditProducts") values (${manager}, ${market}, true, ${notifications || false}, ${canEditProducts || false}) on conflict(manager,market) do update set active = true`);
}

async function deleteManager(market, manager) {
    return await knex
        .raw(`insert into managers (manager, market, active) values (${manager}, ${market}, false) on conflict(manager,market) do update set active = false`);
}

async function getManagers(market) {
    return await knex('users')
        .select('users.id','email','telephone','first_name','second_name',
            'discount', 'postcode', 'birth_date', 'country', 'region', 'city',
            'address', 'rating', 'isAdmin', 'isFarmer', 'created_at', 'updated_at', 'canEditProducts')
        .joinRaw(`inner join managers on manager = users.id and market = ${market}`)
        .whereRaw(`users.id in (select manager from managers where market = ${market} and active = true)`);

}


async function getManagersEmail(market) {
    return await knex('users')
        .select('email')
        .whereRaw(`id in (select manager from managers where market = ${market} and active = true and notifications = true)`)
        .then(res => {
            for (let i = 0; i < res.length; i++) res[i] = res[i].email;
            return res;
        });

}

async function getMy(userId, onlyOwner, onlyActive) {

    let where = `(owner = ${userId} and true = (select "isFarmer" from users where users.id = ${userId})) or (id = any (select market from managers where manager = ${userId} and active = true and "canEditProducts" = true) and markets.active = true)`;
    if (!onlyOwner) where += ` or (id = any (select market from managers where manager = ${userId}) and markets.active = true)`;

    if (onlyActive) where = `(` + where + `) and active = true`;

    return knex('markets')
        .whereRaw(where)
        .select('*');
}

async function getManagerNotifications(user) {
    return await knex('markets')
        .joinRaw(`inner join managers on manager = ${user} and market = markets.id and managers.active = true`)
        .select('markets.id', 'markets.name', 'managers.notifications')
        .orderBy('name', 'asc');
}

async function setManagerNotifications(market, user, notifications) {
    await knex('managers')
        .where({
            manager: user,
            market: market
        })
        .update({
            notifications: notifications
        });

    return await getManagerNotifications(user);
}

async function setCanEditProducts(market, user, flag) {
    return await knex('managers')
        .where({
            manager: user,
            market: market
        })
        .update({
            canEditProducts: flag
        });

}

async function getWhereOwner(userId) {
    return await knex('markets')
        .where({
            owner: userId
        })
        .pluck('id');
}

async function getWhereManager(userId) {
    return await knex('managers')
        .where({
            manager: userId,
            active: true
        })
        .pluck('market');
}

async function getWhereCourier(userId) {
    return await knex('couriers')
        .where({
            courier: userId,
            active: true
        })
        .pluck('market');
}


async function addCourier(market, courier, notifications) {
    return await knex
        .raw(`insert into couriers (courier, market, notifications) values (${courier}, ${market}, ${notifications || false}) on conflict(courier,market) do update set active = true`);
}

async function deleteCourier(market, courier) {
    return await knex
        .raw(`insert into couriers (courier, market, active) values (${courier}, ${market}, false) on conflict(courier, market) do update set active = false`);
}

async function getCouriers(market) {
    return await knex('users')
        .select('users.id','email','telephone','first_name','second_name')
        .joinRaw(`inner join couriers on courier = users.id and market = ${market}`)
        .whereRaw(`users.id in (select courier from couriers where market = ${market} and active = true)`);

}


async function getCourierNotifications(user) {
    return await knex('markets')
        .joinRaw(`inner join couriers on courier = ${user} and market = markets.id and couriers.active = true`)
        .select('markets.id', 'markets.name', 'couriers.notifications')
        .orderBy('name', 'asc');
}

async function setCourierNotifications(market, user, notifications) {
    await knex('couriers')
        .where({
            courier: user,
            market: market
        })
        .update({
            notifications: notifications
        });

    return await getCourierNotifications(user);
}



module.exports = {
    getMany,
    getOne,
    add,
    edit,
    isOwner,
    addManager,
    deleteManager,
    getManagers,
    isManager,
    isManagerBySuborder,
    isOwnerBySuborder,
    getMy,
    getManagersEmail,
    getManagerNotifications,
    setManagerNotifications,
    setCanEditProducts,
    addCourier,
    deleteCourier,
    getCouriers,
    getCourierNotifications,
    setCourierNotifications,
    getWhereOwner,
    getWhereManager,
    getWhereCourier
};
