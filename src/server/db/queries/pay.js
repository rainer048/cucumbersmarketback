const knex = require('../connection');

async function getTransaction(id) {
    return await knex('pay_transactions')
        .where({
            id: id
        })
        .first()
        .select('*');
}

async function addTransaction(data) {
    return await knex('pay_transactions')
        .insert({
            amount: data.amount,
            withdraw_amount: data.withdraw_amount,
            unaccepted: data.unaccepted,
            label: data.label,
            datetime: data.datetime,
            sha1_hash: data.sha1_hash,
            operation_label: data.operation_label,
            operation_id: data.operation_id,
            currency: data.currency
        });

}

async function changePayStatus(orderId, amount) {

    console.log(orderId);

    let order = await knex('orders')
        .select('*')
        .where({
            id: orderId
        })
        .first();

    console.log(order);

    if (order.sum === amount) {
        await knex('orders')
            .update({
                pay_status: 2
            })
            .where({
                id: orderId
            });
        return true;
    } else {
        await knex('orders')
            .update({
                pay_status: 3
            })
            .where({
                id: orderId
            });
        return false;
    }

}

module.exports = {
    getTransaction,
    addTransaction,
    changePayStatus
};
