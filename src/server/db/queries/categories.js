const knex = require('../connection');

async function addCategory(name) {
    return await knex('product_categories')
        .insert({
            name: name
        })
        .returning('*')
        .then(res => res[0]);
}

async function getOne(id) {

    let product = await knex('product_categories')
        .where({
            id: id
        })
        .select('*')
        .first();
    return product;
}

async function getAllCategories(forEdit) {

    return forEdit ?
        await knex('product_categories')
            .select('*')
            .orderBy('id', 'asc') :
        await knex('product_categories')
            .whereRaw('id = any (select category from products where active = true  and true = (select active from markets where id = products.market))')
            .select('*')
            .orderBy('id', 'asc');
}

function editCategory(id, data) {
    return knex('product_categories')
        .where({
            id: id
        })
        .update(data)
        .returning('*')
        .then(res => res[0]);
}

function deleteCategory(id) {
    return knex('product_categories')
        .where({
            id: id
        })
        .del();
}

module.exports = {
    addCategory,
    getAllCategories,
    getOne,
    editCategory,
    deleteCategory
};
