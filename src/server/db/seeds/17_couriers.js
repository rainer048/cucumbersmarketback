exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('couriers').del()
        .then(function () {
            // Inserts seed entries
            return knex('couriers').insert([
                {
                    courier: 1,
                    market: 3,
                    active: true,
                    notifications: true
                },
                {
                    courier: 2,
                    market: 3,
                    notifications: true
                }
            ]);
        });
};

