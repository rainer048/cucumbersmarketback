exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('faq').del()
        .then(function () {
            // Inserts seed entries
            return knex('faq').insert([
                {
                    topic: 1,
                    question: 'Вопрос о доставке 1',
                    answer: 'Ответ на вопрос о доставке 1'
                },
                {
                    topic: 1,
                    question: 'Вопрос о доставке 2',
                    answer: 'Ответ на вопрос о доставке 2'
                },
                {
                    topic: 1,
                    question: 'Вопрос о доставке 3',
                    answer: 'Ответ на вопрос о доставке 3'
                },
                {
                    topic: 2,
                    question: 'Вопрос об оплате 1',
                    answer: 'Ответ на вопрос об оплате 1'
                },
                {
                    topic: 2,
                    question: 'Вопрос об оплате 2',
                    answer: 'Ответ на вопрос об оплате 2'
                },
                {
                    topic: 2,
                    question: 'Вопрос об оплате 3',
                    answer: 'Ответ на вопрос об оплате 3'
                },
                {
                    topic: 3,
                    question: 'Вопрос о продукции 1',
                    answer: 'Ответ на вопрос о продукции 1'
                },
                {
                    topic: 3,
                    question: 'Вопрос о продукции 2',
                    answer: 'Ответ на вопрос о продукции 2'
                },
                {
                    topic: 3,
                    question: 'Вопрос о продукции 3',
                    answer: 'Ответ на вопрос о продукции 3'
                },
                {
                    topic: 4,
                    question: 'Прочий вопрос 1',
                    answer: 'Ответ на прочий вопрос 1'
                },
                {
                    topic: 4,
                    question: 'Прочий вопрос 2',
                    answer: 'Ответ на прочий вопрос 2'
                },
                {
                    topic: 4,
                    question: 'Прочий вопрос 3',
                    answer: 'Ответ на прочий вопрос 3'
                }

            ]);
        });
};

