exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('suborder_statuses').del()
        .then(function () {
            // Inserts seed entries
            return knex('suborder_statuses').insert([

                {
                    status: 'На рассмотрении',
                    status_description: 'Заказ сформирован. Ожидание подтверждения менеджером магазина.'
                },
                {
                    status: 'Согласован',
                    status_description: 'Заказ согласован'
                },
                {
                    status: 'Отклонен оператором',
                    status_description: 'Заказ отклонен оператором'
                },
                {
                    status: 'Отклонен системой',
                    status_description: 'Заказ отклонен системой'
                },
                {
                    status: 'Отменен заказчиком',
                    status_description: 'Заказ отменен заказчиком.'
                },
                {
                    status: 'Отменен оператором',
                    status_description: 'Заказ отменен оператором.'
                }

            ]);
        });
};
