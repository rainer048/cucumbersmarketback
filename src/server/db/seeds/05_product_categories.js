exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('product_categories').del()
        .then(function () {
            // Inserts seed entries
            return knex('product_categories').insert([

                {
                    name: 'Огурцы'
                },
                {
                    name: 'Томаты'
                },
                {
                    name: 'Свекла'
                },
                {
                    name: 'Морковь'
                },
                {
                    name: 'Картофель'
                },
                {
                    name: 'Тыква'
                },
                {
                    name: 'Вишня'
                },
                {
                    name: 'Горох'
                }

            ]);
        });
};
