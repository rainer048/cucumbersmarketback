exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('feedback').del()
        .then(function () {
            // Inserts seed entries
            return knex('feedback').insert([
                {
                    name: 'User 1',
                    email: 'user1@gmail.com',
                    telephone: '88005550001',
                    message: 'Как платить???'
                },
                {
                    name: 'User 2',
                    email: 'user2@gmail.com',
                    telephone: '88005550002',
                    message: 'Как забирать?!'
                }
            ]);
        });
};

