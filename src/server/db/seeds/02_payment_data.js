exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('payment_data').del()
    .then(function () {
      // Inserts seed entries
      return knex('payment_data').insert([
          {
              payment_system: 1,
              data: '4323758329571280'
          },
          {
              payment_system: 1,
              data: '7583958275291847'
          },
          {
              payment_system: 2,
              data: '1237382947190'
          },
          {
              payment_system: 2,
              data: '4727828492813'
          },
          {
              payment_system: 1,
              data: '8472647291047264'
          }
      ]);
    });
};
