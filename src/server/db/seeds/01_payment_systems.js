exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('payment_systems').del()
    .then(function () {
      // Inserts seed entries
      return knex('payment_systems').insert([

          {
              name: 'Сбербанк',
              abbr: 'СБ',
              description: 'Оплата на карту Сбербанка'
          },
          {
              name: 'Яндекс Деньги',
              abbr: 'ЯД',
              description: 'Оплата на кошелек Яндекс Деньги'
          },
          {
              name: 'Наличными',
              abbr: 'Нал',
              description: 'Оплата наличными'
          }
      ]);
    });
};
