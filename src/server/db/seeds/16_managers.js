exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('managers').del()
        .then(function () {
            // Inserts seed entries
            return knex('managers').insert([
                {
                    manager: 1,
                    market: 3,
                    active: true,
                    canEditProducts: true
                },
                {
                    manager: 2,
                    market: 3,
                    active: true
                }
            ]);
        });
};

