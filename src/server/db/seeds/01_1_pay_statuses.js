exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('pay_statuses').del()
    .then(function () {
      // Inserts seed entries
      return knex('pay_statuses').insert([

          {
              status: 'Ожидание оплаты',
              status_description: 'Ожидание оплаты'
          },
          {
              status: 'Оплачено',
              status_description: 'Заказ успешно оплачен'
          },
          {
              status: 'Ошибка',
              status_description: 'Ошибка оплаты заказа'
          },

      ]);
    });
};
