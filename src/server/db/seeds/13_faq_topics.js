exports.seed = function (knex, Promise) {
    // Deletes ALL existing entries
    return knex('faq_topics').del()
        .then(function () {
            // Inserts seed entries
            return knex('faq_topics').insert([
                {
                    topic: 'Вопросы о доставке',
                },
                {
                    topic: 'Вопросы об оплате',
                },
                {
                    topic: 'Вопросы о продукции',
                },
                {
                    topic: 'Прочие вопросы',
                }
            ]);
        });
};

