exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('markets').del()
    .then(function () {
      // Inserts seed entries
      return knex('markets').insert([
          {
              name: 'Морковь Яна Полуяна',
              country: 'Россия',
              region: 'Ростовская область',
              city: 'Ростов-на-Дону',
              address: 'улица Пушкина, дом 11',
              geoposition: [44.4248668,40.7565863],
              timetable: 'Пн-Пт 08.00-19.00, Сб-Вс 09.00-17.00',
              email: 'cucumbers-tomatoes1@email.com',
              telephones: ['88005553531', '88005453531', '88005253521', '88005553521', '88005553511'],
              payment: ['cash', 'cashless'],
              ym_number: '4100110336113971',
              delivery: ['pickup', 'regions_delivery'],
              allowed_delivery_addresses: ['По всей России'],
              owner: 1,
              active: true
          },
          {
              name: 'Магазин "Овощной"',
              country: 'Россия',
              region: 'Ростовская область',
              city: 'Батайск',
              address: 'улица Картонная, дом 2',
              geoposition: [44.4248668,40.7565863],
              timetable: 'Пн-Пт 08.00-19.00, Сб-Вс 09.00-17.00',
              email: 'cucumbers-tomatoes2@email.com',
              telephones: ['88005553521', '88005553511'],
              payment: ['cash', 'cashless'],
              ym_number: '4100110336113971',
              delivery: ['pickup', 'regions_delivery'],
              allowed_delivery_addresses: ['Ростов-на-Дону'],
              owner: 1,
              active: true

          },
          {
              name: 'Севиль',
              country: 'Россия',
              region: 'Краснодарский край',
              city: 'Псебай',
              address: 'улица Строителей, дом 34',
              geoposition: [44.4248668,40.7565863],
              timetable: 'Пн-Пт 08.00-19.00, Сб-Вс 09.00-17.00',
              email: 'cucumbers-tomatoes3@email.com',
              telephones: ['88005553531', '88005453531', '88005553511'],
              payment: ['cash', 'cashless'],
              ym_number: '4100110336113971',
              delivery: ['pickup', 'regions_delivery'],
              allowed_delivery_addresses: ['Валентинск', 'Адамовск'],
              owner: 8,
              active: true
          },
          {
              name: 'Овощной Магазин "У Шурика"',
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Арзамас',
              address: 'улица Ленина, дом 84',
              geoposition: [44.4248668,40.7565863],
              timetable: 'Пн-Пт 08.00-19.00, Сб-Вс 09.00-17.00',
              email: 'cucumbers-tomatoes4@email.com',
              telephones: ['88005553531', '88005453531', '88005253521', '88005553511'],
              payment: ['cash'],
              delivery: ['pickup', 'regions_delivery'],
              allowed_delivery_addresses: ['Псебай', 'Холуй', 'Батай'],
              owner: 8,
              active: false
          },
          {
              name: 'Магазин Фрукты Овощи ,,ФРУКТОВЫЙ РАЙ\'\'',
              country: 'Россия',
              region: 'Ленинградская обл.',
              city: 'Пушкин',
              address: 'улица Есенина, дом 123',
              geoposition: [44.4248668,40.7565863],
              timetable: 'Пн-Пт 08.00-19.00, Сб-Вс 09.00-17.00',
              email: 'cucumbers-tomatoes5@email.com',
              telephones: ['88005553531', '88005453531', '88005253521', '88005553511'],
              payment: ['cashless'],
              ym_number: '4100110336113971',
              delivery: ['pickup', 'regions_delivery'],
              allowed_delivery_addresses: ['Валетск', 'Дамовск', 'Королевск'],
              owner: 10,
              active: true
          },
      ]);
    });
};
