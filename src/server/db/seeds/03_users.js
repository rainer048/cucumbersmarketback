exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
          {
              email: 'user1@email.com',
              telephone: '89299271685',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Карп',
              second_name: 'Смирнов',
              discount: 5,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7',
              isFarmer: true,
              isManager: true
          },
          {
              email: 'user2@email.com',
              telephone: '89298208185',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Леонид',
              second_name: 'Кузнецов',
              discount: 3,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7',
              isAdmin: true,
              isManager: true
          },
          {
              email: 'user3@email.com',
              telephone: '89291748695',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Никанор',
              second_name: 'Новиков',
              discount: 5,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7'
          },
          {
              email: 'user4@email.com',
              telephone: '89995965515',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Матвей',
              second_name: 'Павлов',
              discount: 1,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7'
          },
          {
              email: 'user5@email.com',
              telephone: '89996808802',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Федот',
              second_name: 'Степанов',
              discount: 2,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7'
          },
          {
              email: 'user6@email.com',
              telephone: '89301066036',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Святозар',
              second_name: 'Андреев',
              discount: 5,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7'
          },
          {
              email: 'user7@email.com',
              telephone: '89301503072',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Иннокентий',
              second_name: 'Елагин',
              discount: 4,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7'
          },
          {
              email: 'user8@email.com',
              telephone: '89101787884',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Кузьма',
              second_name: 'Селиверстов',
              discount: 7,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7',
              isFarmer: true
          },
          {
              email: 'user9@email.com',
              telephone: '89109031987',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Дементий',
              second_name: 'Шереметев',
              discount: 1,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7'
          },
          {
              email: 'user10@email.com',
              telephone: '89045249568',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Платон',
              second_name: 'Токарев',
              discount: 10,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7',
              isFarmer: true
          },
          {
              email: 'user11@email.com',
              telephone: '89044875158',
              password: '$2b$10$BnyorHdRkSq/TNi.RAvphOvji6AN/tJrNi7b2xSwII1fqFIJXDPlW',
              first_name: 'Родион',
              second_name: 'Листопадов',
              discount: 17,
              postcode: 603000,
              birth_date: 318211200,
              country: 'Россия',
              region: 'Нижегородская область',
              city: 'Нижний Новгород',
              address: 'пр. Гагарина, д. 168, кв.7'
          }
      ]);
    });
};
