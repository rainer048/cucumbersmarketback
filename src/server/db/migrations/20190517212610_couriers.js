const { onUpdateCouriersTrigger } = require('../../../../knexfile');
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('couriers', function (table) {
            table.increments('id').primary();
            table.integer('courier').references('id').inTable('users');
            table.integer('market').references('id').inTable('markets');
            table.boolean('active').defaultTo(true);
            table.boolean('notifications').defaultTo(false).notNullable();
            table.unique(['courier', 'market']);
        })
        .then(() => knex.raw(onUpdateCouriersTrigger('couriers')));
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("couriers");
};
