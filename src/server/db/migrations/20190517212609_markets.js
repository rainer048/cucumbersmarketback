const { onUpdateTrigger } = require('../../../../knexfile');
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('markets', function (table) {
            table.increments('id').primary();
            table.string('name', 255).notNullable();
            table.string('country', 255).defaultTo(null);
            table.string('region', 255).defaultTo(null);
            table.string('city', 255).defaultTo(null);
            table.string('address', 255).defaultTo(null);
            table.specificType('geoposition', 'double precision [] CHECK (array_length(geoposition, 1) = 2)').defaultTo(null);
            table.text('timetable', 'longtext');
            table.string('email', 255).notNullable();
            table.specificType('telephones', 'varchar(16) [] CHECK (array_length(telephones, 1) < 7)');
            table.specificType('payment', 'varchar(64) []').defaultTo('{}');
            table.string('ym_number').defaultTo(null);
            table.specificType('delivery', 'varchar(64) []').defaultTo('{}');
            table.specificType('allowed_delivery_addresses', 'varchar(128) []').defaultTo('{}');
            table.integer('owner').references('id').inTable('users');
            table.boolean('active').defaultTo(false).notNullable();
            table.integer('created_at').defaultTo(knex.raw('extract(epoch from now())'));
            table.integer('updated_at').defaultTo(knex.raw('extract(epoch from now())'));

        })
        .then(() => knex.raw(onUpdateTrigger('markets')));
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("markets");
};
