
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('payment_systems', function (table) {
            table.increments('id').primary();
            table.string('name', 255).notNullable();
            table.string('abbr', 255).notNullable();
            table.string('description', 255).notNullable();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("payment_systems");
};
