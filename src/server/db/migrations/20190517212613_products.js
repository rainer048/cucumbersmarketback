const { onUpdateTrigger } = require('../../../../knexfile');
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('products', function (table) {
            table.increments('id').primary();
            table.string('name', 255).notNullable();
            table.integer('category').references('id').inTable('product_categories').notNullable();
            table.specificType('sort', 'sort_type').defaultTo(null);
            table.text('description', 'longtext').defaultTo(null);
            table.integer('limit');
            table.string('image', 255).defaultTo('/image/0.jpg');
            table.integer('market').references('id').inTable('markets').notNullable();
            table.integer('total_sold').defaultTo(0);
            table.boolean('wholesale').defaultTo(true).notNullable();
            table.boolean('retail').defaultTo(false).notNullable();
            table.integer('retail_price').defaultTo(0);
            table.boolean('active').defaultTo(false).notNullable();
            table.integer('created_at').defaultTo(knex.raw('extract(epoch from now())'));
            table.integer('updated_at').defaultTo(knex.raw('extract(epoch from now())'));

        })
        .then(() => knex.raw(onUpdateTrigger('products')));
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("products");
};
