exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('product_categories', function (table) {
            table.increments('id').primary();
            table.string('name', 255).unique().notNullable();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("product_categories");
};
