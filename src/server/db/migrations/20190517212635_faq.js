
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('faq', function (table) {
            table.increments('id').primary();
            table.integer('topic').references('id').inTable('faq_topics').onDelete('cascade').notNullable();
            table.text('question', 'longtext').notNullable();
            table.text('answer', 'longtext').notNullable();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("faq");
};
