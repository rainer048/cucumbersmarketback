const { onUpdateManagersTrigger } = require('../../../../knexfile');
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('managers', function (table) {
            table.increments('id').primary();
            table.integer('manager').references('id').inTable('users');
            table.integer('market').references('id').inTable('markets');
            table.boolean('active').defaultTo(true);
            table.boolean('canEditProducts').defaultTo(false);
            table.boolean('notifications').defaultTo(false).notNullable();
            table.unique(['manager', 'market']);
        })
        .then(() => knex.raw(onUpdateManagersTrigger('managers')));
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("managers");
};
