
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('info', function (table) {
            table.increments('id').primary();
            table.string('topic', 255).notNullable();
            table.string('name', 255).unique().notNullable();
            table.text('text', 'longtext').notNullable();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("info");
};
