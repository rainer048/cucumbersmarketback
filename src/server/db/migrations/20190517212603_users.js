const { onUpdateTrigger } = require('../../../../knexfile');
exports.up = function(knex, Promise) {


    return knex.schema
        .createTable('users', function (table) {
            table.increments('id').primary();
            table.string('email', 255).unique();
            table.string('telephone', 255).unique();
            table.string('password', 255).notNullable();
            table.string('first_name', 255).notNullable();
            table.string('second_name', 255).notNullable();
            table.integer('discount').defaultTo(0).notNullable();
            table.string('refresh_token', 255).defaultTo('').notNullable();
            table.integer('postcode', 6).defaultTo(null);
            table.integer('birth_date').defaultTo(null);
            table.string('country', 255).defaultTo(null);
            table.string('region', 255).defaultTo(null);
            table.string('city', 255).defaultTo(null);
            table.string('address', 255).defaultTo(null);
            table.integer('rating').defaultTo(0).notNullable();
            table.boolean('isAdmin').defaultTo(false).notNullable();
            table.boolean('isFarmer').defaultTo(false).notNullable();
            table.boolean('becomeFarmer').defaultTo(false).notNullable();
            table.boolean('isManager').defaultTo(false).notNullable();
            table.boolean('isCourier').defaultTo(false).notNullable();
            table.boolean('notifications').defaultTo(false).notNullable();
            table.boolean('subscription').defaultTo(false).notNullable();
            table.integer('created_at').defaultTo(knex.raw('extract(epoch from now())'));
            table.integer('updated_at').defaultTo(knex.raw('extract(epoch from now())'));

        })
        .then(() => knex.raw(onUpdateTrigger('users')));
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("users");
};
