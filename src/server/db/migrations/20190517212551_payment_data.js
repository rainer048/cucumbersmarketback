exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('payment_data', function (table) {
            table.increments('id').primary();
            table.integer('payment_system').references('id').inTable('payment_systems');
            table.string('data', 255).notNullable();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("payment_data");
};
