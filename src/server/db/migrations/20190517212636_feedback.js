
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('feedback', function (table) {
            table.increments('id').primary();
            table.string('name', 255).notNullable();
            table.string('email', 255).notNullable();
            table.string('telephone', 255).notNullable();
            table.text('message', 'longtext').notNullable();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("feedback");
};
