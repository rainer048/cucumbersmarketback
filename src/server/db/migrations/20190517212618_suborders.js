const { onUpdateTrigger } = require('../../../../knexfile');
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('suborders', function (table) {
            table.string('id').notNullable().primary();
            table.integer('product').references('id').inTable('products').notNullable();
            table.integer('count').notNullable();
            table.integer('cost').notNullable();
            table.integer('status').references('id').inTable('suborder_statuses').defaultTo(1).notNullable();
            table.integer('shipping_date').defaultTo(null);
            table.integer('created_at').defaultTo(knex.raw('extract(epoch from now())'));
            table.integer('updated_at').defaultTo(knex.raw('extract(epoch from now())'));

        })
        .then(() => knex.raw(onUpdateTrigger('suborders')));
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("suborders");
};
