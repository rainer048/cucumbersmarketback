const { onUpdateTrigger } = require('../../../../knexfile');
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('orders', function (table) {
            table.increments('id').primary();
            table.integer('customer').references('id').inTable('users').notNullable();
            table.specificType('suborders', 'VARCHAR(64) []');
            table.integer('sum').defaultTo(0);
            table.integer('status').references('id').inTable('order_statuses').defaultTo(1).notNullable();
            table.string('type').notNullable();
            table.string('pay_type').notNullable().defaultTo('CASH');
            table.integer('pay_status').references('id').inTable('pay_statuses').defaultTo(1);
            table.integer('pay_transaction').references('id').inTable('pay_transactions').defaultTo(null);
            table.string('delivery_type').defaultTo(null);
            table.string('delivery_address').defaultTo(null);
            table.integer('created_at').defaultTo(knex.raw('extract(epoch from now())'));
            table.integer('updated_at').defaultTo(knex.raw('extract(epoch from now())'));
        })
        .then(() => knex.raw(onUpdateTrigger('orders')));
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("orders");
};
