const ON_UPDATE_MANAGERS_FUNCTION = `
  CREATE OR REPLACE FUNCTION on_update_managers()
  RETURNS trigger AS $$
  BEGIN
    UPDATE users SET "isManager" = exists(select id from managers where manager=NEW.manager and active=true) WHERE users.id = NEW.manager;
    RETURN NEW;
  END;
$$ language 'plpgsql';
`;

const DROP_ON_UPDATE_MANAGERS_FUNCTION = `DROP FUNCTION on_update_managers`;

exports.up = knex => knex.raw(ON_UPDATE_MANAGERS_FUNCTION);
exports.down = knex => knex.raw(DROP_ON_UPDATE_MANAGERS_FUNCTION);
