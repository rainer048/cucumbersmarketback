const TYPE_OF_SORTS = `
  CREATE TYPE sort_type AS ENUM ('3', '2', '1', 'Высший');;
`;

exports.up = knex => knex.raw(TYPE_OF_SORTS);
exports.down = knex => knex.raw('drop type sort_type');
