
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('faq_topics', function (table) {
            table.increments('id').primary();
            table.string('topic', 255).unique().notNullable();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("faq_topics");
};
