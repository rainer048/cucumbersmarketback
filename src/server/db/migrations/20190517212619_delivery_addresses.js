exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('delivery_addresses', function (table) {
            table.increments('id').primary();
            table.integer('owner').references('id').inTable('users').notNullable();
            table.string('region');
            table.string('locality');
            table.string('street');
            table.string('house');
            table.string('apartment');
            table.string('first_name');
            table.string('second_name');
            table.string('phone_number');
            table.string('email');
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("delivery_addresses");
};
