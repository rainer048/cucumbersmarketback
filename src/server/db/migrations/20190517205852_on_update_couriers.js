const ON_UPDATE_COURIERS_FUNCTION = `
  CREATE OR REPLACE FUNCTION on_update_couriers()
  RETURNS trigger AS $$
  BEGIN
    UPDATE users SET "isCourier" = exists(select id from couriers where courier=NEW.courier and active=true) WHERE users.id = NEW.courier;
    RETURN NEW;
  END;
$$ language 'plpgsql';
`;

const DROP_ON_UPDATE_COURIERS_FUNCTION = `DROP FUNCTION on_update_couriers`;

exports.up = knex => knex.raw(ON_UPDATE_COURIERS_FUNCTION);
exports.down = knex => knex.raw(DROP_ON_UPDATE_COURIERS_FUNCTION);
