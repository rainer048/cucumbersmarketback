exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('pay_statuses', function (table) {
            table.increments('id').primary();
            table.string('status', 255).unique().notNullable();
            table.string('status_description', 255).unique().defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("pay_statuses");
};
