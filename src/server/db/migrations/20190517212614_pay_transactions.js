const { onUpdateTrigger } = require('../../../../knexfile');
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('pay_transactions', function (table) {
            table.increments('id').primary();
            table.float('amount');
            table.float('withdraw_amount');
            table.boolean('unaccepted');
            table.string('label');
            table.dateTime('datetime');
            table.string('sha1_hash');
            table.string('operation_label');
            table.string('operation_id');
            table.integer('currency');
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("pay_transactions");
};
