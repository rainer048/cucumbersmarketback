const Router = require('koa-router');
const validator = require('../helpers/validator');
const marketsQueries = require('../db/queries/markets');
const authHelper = require('../helpers/auth');
const reportHelper = require('../helpers/report');
const mailerHelper = require('../helpers/mailer');
const moment = require('moment');
const router = new Router();
const PREFIX_URL = '/markets';
const GET_MANY_MARKETS_URL = `${PREFIX_URL}`;
const GET_ONE_MARKET_URL = `${PREFIX_URL}/getOne`;
const ADD_MARKET_URL = `${PREFIX_URL}/add`;
const EDIT_MARKET_URL = `${PREFIX_URL}/edit`;
const GET_WHOLESALE_REPORT_URL = `${PREFIX_URL}/getWholesaleReport`;
const GET_RETAIL_REPORT_URL = `${PREFIX_URL}/getRetailReport`;
const ADD_MANAGER_URL = `${PREFIX_URL}/addManager`;
const DELETE_MANAGER_URL = `${PREFIX_URL}/deleteManager`;
const GET_MANAGERS_URL = `${PREFIX_URL}/getManagers`;
const GET_MY_MARKETS_URL = `${PREFIX_URL}/getMyMarkets`;
const GET_MANAGER_NOTIFICATIONS = `${PREFIX_URL}/getManagerNotifications`;
const SET_MANAGER_NOTIFICATIONS = `${PREFIX_URL}/setManagerNotifications`;
const SET_CAN_EDIT_PRODUCTS_URL = `${PREFIX_URL}/setCanEditProducts`;
const ADD_COURIER_URL = `${PREFIX_URL}/addCourier`;
const DELETE_COURIER_URL = `${PREFIX_URL}/deleteCourier`;
const GET_COURIERS_URL = `${PREFIX_URL}/getCouriers`;
const GET_COURIER_NOTIFICATIONS = `${PREFIX_URL}/getCourierNotifications`;
const SET_COURIER_NOTIFICATIONS = `${PREFIX_URL}/setCourierNotifications`;

router.post(GET_MANY_MARKETS_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_MANY_MARKETS_SCHEMA),
    async (ctx) => {

    try {
        let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
        let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
        let admin = ctx.state.user ? ctx.state.user.isAdmin : false;
        let markets = await marketsQueries.getMany(limit, offset, admin);
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'Точки продажи получены',
            data: markets.markets,
            total: markets.total,
            loggedIn: ctx.state.loggedIn
        };
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_ONE_MARKET_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_ONE_MARKET_SCHEMA),
    async (ctx) => {

    try {
        let market = await marketsQueries.getOne(ctx.request.body.id);
        if (market) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Точка продаж получена',
                data: market,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Точка продаж не найдена',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(ADD_MARKET_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.ADD_MARKET_SCHEMA),
    async (ctx) => {

    try {
        let market = await marketsQueries.add(ctx.state.user.id, ctx.request.body);
        if (market) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Точка продаж добавлена',
                data: market,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err);
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(EDIT_MARKET_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.EDIT_MARKET_SCHEMA),
    async (ctx) => {

    try {
        let res = await marketsQueries.edit(ctx.request.body.id, ctx.request.body.data);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Точка продажи изменена',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Точка продажи не найдена',
                loggedIn: ctx.state.loggedIn
            }
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

let getReport = (type) => async (ctx, next) => {

    let marketId = ctx.request.body.market;
    let orderBy = ctx.request.body.orderBy || 'id';
    let order = ctx.request.body.order || 'asc';
    let fromTime = ctx.request.body.fromTime || moment().startOf('day').unix();
    let toTime = ctx.request.body.toTime || moment().endOf('day').unix();
    let sendToEmail = ctx.request.body.sendToEmail || false;
    if (! (await marketsQueries.isOwner(marketId, ctx.state.user.id) || (await marketsQueries.isManager(marketId, ctx.state.user.id)))) {
        ctx.status = 403;
        ctx.body = {
            status: 'Forbidden',
            message: 'Пользователь не является владельцем точки продажи!',
            loggedIn: ctx.state.loggedIn
        };
        return;
    }

    try {
        await reportHelper.getReport(marketId, fromTime, toTime, orderBy, order, type).then( async res => {

            if (sendToEmail) {
                await mailerHelper.sendReport(ctx.state.user.email, res);
            }

            ctx.status = 200;
            ctx.body = {
                status: 'ok',
                message: 'Pdf документ отправлен!',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        });
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
    return next();
};

router.post(GET_WHOLESALE_REPORT_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_REPORT_SCHEMA),
    getReport('WHOLESALE')
);

router.post(GET_RETAIL_REPORT_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_REPORT_SCHEMA),
    getReport('RETAIL')
);

router.post(ADD_MANAGER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.ADD_MANAGER_SCHEMA),
    async (ctx) => {

    let userId = ctx.state.user.id;
    let manager = ctx.request.body.manager;
    let market = ctx.request.body.market;

    try {
        if (!(await marketsQueries.isOwner(market, userId))) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden',
                message: 'Пользователь не является владельцем точки продажи!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        } else {
            let res = await marketsQueries.addManager(market, manager);
            if (res) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Оператор добавлен',
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Точка продажи не найдена',
                    loggedIn: ctx.state.loggedIn
                }
            }
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(DELETE_MANAGER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DELETE_MANAGER_SCHEMA),
    async (ctx) => {

    let userId = ctx.state.user.id;
    let manager = ctx.request.body.manager;
    let market = ctx.request.body.market;

    try {
        if (!(await marketsQueries.isOwner(market, userId))) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden',
                message: 'Пользователь не является владельцем точки продажи!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        } else {
            let res = await marketsQueries.deleteManager(market, manager);
            if (res) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Оператор удален',
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Точка продажи не найдена',
                    loggedIn: ctx.state.loggedIn
                }
            }
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_MANAGERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_MANAGERS_SCHEMA),
    async (ctx) => {

    let userId = ctx.state.user.id;
    let market = ctx.request.body.market;

    try {
        if (!(await marketsQueries.isOwner(market, userId))) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden',
                message: 'Пользователь не является владельцем точки продажи!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        } else {
            let res = await marketsQueries.getManagers(market);
            if (res) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Идентификаторы операторов получены',
                    data: res,
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Точка продажи не найдена',
                    loggedIn: ctx.state.loggedIn
                }
            }
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_MY_MARKETS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_MY_MARKETS_SCHEMA),
    async (ctx) => {
        let userId = ctx.state.user.id;
        let onlyOwner = ctx.request.body.onlyOwner || false;
        let onlyActive = ctx.request.body.onlyActive || false;

        try {
            let markets = await marketsQueries.getMy(userId, onlyOwner, onlyActive);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Точки продажи получены',
                data: markets,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(GET_MANAGER_NOTIFICATIONS,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_MANAGER_NOTIFICATIONS_SCHEMA),
    async (ctx) => {
        let user = ctx.state.user;
        if (!user.isManager) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden!',
                message: 'Пользователь не является оператором!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        }
        try {
            let data = await marketsQueries.getManagerNotifications(user.id);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Статусы уведомлений получены',
                data: data,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(SET_MANAGER_NOTIFICATIONS,
    authHelper.checkAuthorization,
    validator.validate(validator.SET_MANAGER_NOTIFICATIONS_SCHEMA),
    async (ctx) => {
        let user = ctx.state.user;
        if (!user.isManager) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden!',
                message: 'Пользователь не является оператором!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        }
        let market = ctx.request.body.market;
        let notifications = ctx.request.body.notifications;
        try {
            let data = await marketsQueries.setManagerNotifications(market, user.id, notifications);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Статус уведомлений установлен',
                data: data,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });


router.post(SET_CAN_EDIT_PRODUCTS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.SET_CAN_EDIT_PRODUCTS_SCHEMA),
    async (ctx) => {
        let market = ctx.request.body.market;
        let manager = ctx.request.body.manager;
        let canEditProducts = ctx.request.body.canEditProducts;

        let user = ctx.state.user;

        if (!user.isFarmer || !(await marketsQueries.isOwner(market, user.id))) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden!',
                message: 'Пользователь не является хозяином точки продаж!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        }
        try {
            const data = await marketsQueries.setCanEditProducts(market, manager, canEditProducts);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Уровень доступа оператора установлен',
                data: data,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });


router.post(ADD_COURIER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.ADD_COURIER_SCHEMA),
    async (ctx) => {

        let userId = ctx.state.user.id;
        let courier = ctx.request.body.courier;
        let market = ctx.request.body.market;

        try {
            if (!(await marketsQueries.isOwner(market, userId))) {
                ctx.status = 403;
                ctx.body = {
                    status: 'Forbidden',
                    message: 'Пользователь не является владельцем точки продажи!',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            } else {
                let res = await marketsQueries.addCourier(market, courier);
                if (res) {
                    ctx.status = 200;
                    ctx.body = {
                        status: 'success',
                        message: 'Курьер добавлен',
                        loggedIn: ctx.state.loggedIn
                    };
                } else {
                    ctx.status = 404;
                    ctx.body = {
                        status: 'error',
                        message: 'Точка продажи не найдена',
                        loggedIn: ctx.state.loggedIn
                    }
                }
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(DELETE_COURIER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DELETE_COURIER_SCHEMA),
    async (ctx) => {

        let userId = ctx.state.user.id;
        let courier = ctx.request.body.courier;
        let market = ctx.request.body.market;

        try {
            if (!(await marketsQueries.isOwner(market, userId))) {
                ctx.status = 403;
                ctx.body = {
                    status: 'Forbidden',
                    message: 'Пользователь не является владельцем точки продажи!',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            } else {
                let res = await marketsQueries.deleteCourier(market, courier);
                if (res) {
                    ctx.status = 200;
                    ctx.body = {
                        status: 'success',
                        message: 'Курьер удален',
                        loggedIn: ctx.state.loggedIn
                    };
                } else {
                    ctx.status = 404;
                    ctx.body = {
                        status: 'error',
                        message: 'Точка продажи не найдена',
                        loggedIn: ctx.state.loggedIn
                    }
                }
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(GET_COURIERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_COURIERS_SCHEMA),
    async (ctx) => {

        let userId = ctx.state.user.id;
        let market = ctx.request.body.market;

        try {
            if (!(await marketsQueries.isOwner(market, userId))) {
                ctx.status = 403;
                ctx.body = {
                    status: 'Forbidden',
                    message: 'Пользователь не является владельцем точки продажи!',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            } else {
                let res = await marketsQueries.getCouriers(market);
                if (res) {
                    ctx.status = 200;
                    ctx.body = {
                        status: 'success',
                        message: 'Идентификаторы курьеров получены',
                        data: res,
                        loggedIn: ctx.state.loggedIn
                    };
                } else {
                    ctx.status = 404;
                    ctx.body = {
                        status: 'error',
                        message: 'Точка продажи не найдена',
                        loggedIn: ctx.state.loggedIn
                    }
                }
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });


router.post(GET_COURIER_NOTIFICATIONS,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_COURIER_NOTIFICATIONS_SCHEMA),
    async (ctx) => {
        let user = ctx.state.user;
        if (!user.isCourier) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden!',
                message: 'Пользователь не является курьером!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        }
        try {
            let data = await marketsQueries.getCourierNotifications(user.id);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Статусы уведомлений получены',
                data: data,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(SET_COURIER_NOTIFICATIONS,
    authHelper.checkAuthorization,
    validator.validate(validator.SET_COURIER_NOTIFICATIONS_SCHEMA),
    async (ctx) => {
        let user = ctx.state.user;
        if (!user.isCourier) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden!',
                message: 'Пользователь не является курьером!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        }
        let market = ctx.request.body.market;
        let notifications = ctx.request.body.notifications;
        try {
            let data = await marketsQueries.setCourierNotifications(market, user.id, notifications);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Статус уведомлений установлен',
                data: data,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });


module.exports = router;
