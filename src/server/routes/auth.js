const Router = require('koa-router');
const usersQueries = require('../db/queries/users');
const authQueries = require('../db/queries/auth');
const validator = require('../helpers/validator');
const authHelper = require('../helpers/auth');
const mailer = require('../helpers/mailer');
const PREFIX_URL = '/auth';
const AUTH_URL = `${PREFIX_URL}`;
const DESTROY_TOKENS_URL = `${PREFIX_URL}/destroyTokens`;
const CHECK_RECOVERY_HASH_URL = `${PREFIX_URL}/checkRecoveryHash`;
const SEND_RECOVERY_HASH_URL = `${PREFIX_URL}/sendRecoveryHash`;
const SET_NEW_PASSWORD = `${PREFIX_URL}/setNewPassword`;
const CHECK_URL = `${PREFIX_URL}/check`;
const CHANGE_PASSWORD_URL = `${PREFIX_URL}/changePassword`;


const router = new Router();


router.post(AUTH_URL,
    validator.validate(validator.AUTH_SCHEMA),
    async (ctx) => {

    let login = ctx.request.body.login;
    let loginData = login.includes('@') ? {'email': login.toLowerCase()} : {'telephone': login};
    let password = ctx.request.body.password;

    try {

        let user = await usersQueries.getUser(loginData);
        if (!user) {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Пользователь не найден!',
                loggedIn: false
            };
        } else {
            let hash = await authQueries.getHash(user.id);
            if (await authHelper.comparePassword(password, hash)) {
                let tokens = await authHelper.updateTokens(user.id);
                let cookiesConfig = authHelper.getCookiesConfig();
                ctx.cookies.set('access_token', tokens.access_token, cookiesConfig.access_token);
                ctx.cookies.set('refresh_token', tokens.refresh_token, cookiesConfig.refresh_token);
                ctx.status = 200;
                ctx.body = {
                    status: 'ok',
                    message: 'Пользователь успешно авторизован!',
                    user: user,
                    loggedIn: true
                };
            } else {
                ctx.status = 401;
                ctx.body = {
                    status: 'error',
                    message: 'Неверный пароль!',
                    loggedIn: false
                };
            }

        }

    } catch(err) {
        console.log(err);
    }
});

router.post(DESTROY_TOKENS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DESTROY_TOKENS_SCHEMA),
    async (ctx) => {

    let id = ctx.state.user.id;
    if(!id) {
        ctx.status = 401;
        ctx.body = {
            status: 'error',
            message: 'Неправильный refresh токен. Пользователь не авторизован.',
            loggedIn: false
        };
    }

    let BD_destroy_token = await authQueries.destroyTokens(id);

    // await usersQueries.updateLastActivityTime(id);

    if (BD_destroy_token) {
        ctx.cookies.set('access_token', null);
        ctx.cookies.set('refresh_token', null);
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'Токены уничтожены',
            loggedIn: false
        }
    } else {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: false
        };
    }

});

router.post(SEND_RECOVERY_HASH_URL,
    validator.validate(validator.SEND_RECOVERY_HASH_SCHEMA),
    async (ctx) => {

    let email = ctx.request.body.email;

    try {
        let user = await usersQueries.getUser({email: email});
        if (user) {
            let secret = authHelper.makeSecret([email, user.updated_at]);
            let hash = authHelper.encodeData(email, secret);
            mailer.sendRecoveryCode(email, hash);
            ctx.status = 200;
            ctx.body = {
                status: 'ok',
                message: 'Email сообщение отправлено!',
                loggedIn: false
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Пользователь не найден!',
                loggedIn: false
            };
        }

    } catch (err) {
        console.log(err);
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера',
            loggedIn: false
        };
    }
});

router.post(CHECK_RECOVERY_HASH_URL,
    validator.validate(validator.CHECK_RECOVERY_HASH_SCHEMA),
    async (ctx) => {

    let hash = ctx.request.body.hash;
    let email = ctx.request.body.email;

    try {
        let user = await usersQueries.getUser({email: email});
        let secret = authHelper.makeSecret([email, user.updated_at]);
        let newHash = authHelper.encodeData(email, secret);
        if (newHash === hash) {
            ctx.status = 200;
            ctx.body = {
                status: 'ok',
                message: 'Hash совпадает!',
                loggedIn: false
            };
        } else {
            ctx.status = 400;
            ctx.body = {
                status: 'error',
                message: 'Неправильный hash!',
                loggedIn: false
            };
        }

    } catch (err) {
        console.log(err);
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера',
            loggedIn: false
        };
    }
});

router.post(SET_NEW_PASSWORD,
    validator.validate(validator.SET_NEW_PASSWORD),
    async (ctx) => {

    let hash = ctx.request.body.hash;
    let email = ctx.request.body.email;
    let password = ctx.request.body.password;
    let passwordHash = await authHelper.getPasswordHash(password);
    try {
        let user = await usersQueries.getUser({email: email});
        let secret = authHelper.makeSecret([email, user.updated_at]);
        let newHash = authHelper.encodeData(email, secret);
        if (newHash === hash) {
                await usersQueries.setPassword(user.id, passwordHash);
            ctx.status = 200;
            ctx.body = {
                status: 'ok',
                message: 'Пароль изменен!',
                loggedIn: false
            };
        } else {
            ctx.status = 400;
            ctx.body = {
                status: 'error',
                message: 'Неправильный hash!',
                loggedIn: false
            };
        }

    } catch (err) {
        console.log(err);
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера',
            loggedIn: false
        };
    }
});

router.post(CHECK_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.CHECK_SCHEMA),
    async (ctx) => {

    ctx.status = 200;
    ctx.body = {
        status: 'ok',
        message: 'Проверено!',
        user: ctx.state.user
    };
});


router.post(CHANGE_PASSWORD_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.CHANGE_PASSWORD_SCHEMA),
    async (ctx) => {

        let oldPassword = ctx.request.body.oldPassword;
        let newPassword1 = ctx.request.body.newPassword1;
        let newPassword2 = ctx.request.body.newPassword2;
        let user = ctx.state.user;

        if (!(newPassword1 === newPassword2)) {
            ctx.status = 400;
            ctx.body = {
                status: 'error',
                message: 'Пароль и подтверждение не совпадают!',
                loggedIn: ctx.state.loggedIn
            };
            return;
        } else {
            try {
                let currentPasswordHash = await authQueries.getHash(user.id);
                if (await authHelper.comparePassword(oldPassword, currentPasswordHash)) {
                    let newPasswordHash = await authHelper.getPasswordHash(newPassword1);
                    await usersQueries.setPassword(user.id, newPasswordHash);
                    ctx.status = 200;
                    ctx.body = {
                        status: 'ok',
                        message: 'Пароль изменен!',
                        loggedIn: ctx.state.loggedIn
                    };
                } else {
                    ctx.status = 400;
                    ctx.body = {
                        status: 'error',
                        message: 'Неправильный старый пароль!',
                        loggedIn: ctx.state.loggedIn
                    };
                }

            } catch (err) {
                console.log(err);
                ctx.status = 500;
                ctx.body = {
                    status: 'error',
                    message: 'Внутренняя ошибка сервера',
                    loggedIn: ctx.state.loggedIn
                };
            }
        }

    });

module.exports = router;
