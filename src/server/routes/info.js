const Router = require('koa-router');
const validator = require('../helpers/validator');
const infoQueries = require('../db/queries/info');
const authHelper = require('../helpers/auth');
const router = new Router();
const PREFIX_URL = '/info';
const GET_INFO_URL = `${PREFIX_URL}`;
const ADD_INFO_URL = `${PREFIX_URL}/add`;
const EDIT_INFO_URL = `${PREFIX_URL}/edit`;
const DELETE_INFO_URL = `${PREFIX_URL}/delete`;
const GET_TOPICS_INFO_URL = `${PREFIX_URL}/getTopics`;

router.post(GET_INFO_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_INFO_SCHEMA),
    async (ctx) => {

    try {
        let topic = ctx.request.body.topic;
        let info = await infoQueries.getInfo(topic);
        if (info) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Информация отправлена',
                data: info.text,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Информация не найдена',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(ADD_INFO_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.ADD_INFO_SCHEMA),
    async (ctx) => {

    try {
        let res = await infoQueries.addInfo(ctx.request.body);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Информация добавлена',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(EDIT_INFO_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.EDIT_INFO_SCHEMA),
    async (ctx) => {

    try {
        let topic = ctx.request.body.topic;
        let info = { name: ctx.request.body.name, text: ctx.request.body.text };
        let res = await infoQueries.editInfo(topic, info);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Информация изменена',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Информация не найдена',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});


router.post(DELETE_INFO_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DELETE_INFO_SCHEMA),
    async (ctx) => {

    try {
        let topic = ctx.request.body.topic;
        let res = await infoQueries.deleteInfo(topic);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Информация удалена',
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Информация не найдена',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_TOPICS_INFO_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_TOPICS_INFO_SCHEMA),
    async (ctx) => {

    try {
        let topics = await infoQueries.getAllTopics();
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'Топики получены',
            data: topics,
            loggedIn: ctx.state.loggedIn
        };
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});


module.exports = router;
