const Router = require('koa-router');
const validator = require('../helpers/validator');
const faqQueries = require('../db/queries/faq.js');
const authHelper = require('../helpers/auth');
const router = new Router();
const PREFIX_URL = '/faq';
const GET_FAQ_URL = `${PREFIX_URL}`;
const GET_ONE_FAQ_URL = `${PREFIX_URL}/getOne`;
const GET_FAQ_BY_TOPIC_URL = `${PREFIX_URL}/getByTopic`;
const GET_FAQ_TOPICS_URL = `${PREFIX_URL}/getTopics`;
const ADD_FAQ_URL = `${PREFIX_URL}/add`;
const ADD_FAQ_TOPIC_URL = `${PREFIX_URL}/addTopic`;
const EDIT_FAQ_URL = `${PREFIX_URL}/edit`;
const EDIT_FAQ_TOPIC_URL = `${PREFIX_URL}/editTopic`;
const DELETE_FAQ_URL = `${PREFIX_URL}/delete`;
const DELETE_FAQ_TOPIC_URL = `${PREFIX_URL}/deleteTopic`;


router.post(GET_FAQ_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_FAQ_SCHEMA),
    async (ctx) => {

    try {
        let faq = await faqQueries.getFaq();
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'FAQ отправлены',
            data: faq,
            loggedIn: ctx.state.loggedIn
        };
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_FAQ_TOPICS_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_FAQ_TOPICS_SCHEMA),
    async (ctx) => {

    try {
        let topics = await faqQueries.getFaqTopics();
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'Топики отправлены',
            data: topics,
            loggedIn: ctx.state.loggedIn
        };
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_ONE_FAQ_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_ONE_FAQ_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        let faq = await faqQueries.getFaqById(id);
        if (faq) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'FAQ отправлен!',
                data: faq,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'FAQ не найден!',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_FAQ_BY_TOPIC_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_FAQ_TOPICS_SCHEMA),
    async (ctx) => {

    try {
        let topic = ctx.request.body.topic;
        let faq = await faqQueries.getFaqByTopic(topic);
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'FAQ отправлены',
            data: faq,
            loggedIn: ctx.state.loggedIn
        };
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(ADD_FAQ_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.ADD_FAQ_SCHEMA),
    async (ctx) => {

    try {
        let faq = ctx.request.body;
        let res = await faqQueries.addFaq(faq);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Faq добавлен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(ADD_FAQ_TOPIC_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.ADD_FAQ_TOPIC_SCHEMA),
    async (ctx) => {

    try {
        let topic = ctx.request.body.topic;
        let res = await faqQueries.addFaqTopic(topic);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Топик добавлен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(EDIT_FAQ_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.EDIT_FAQ_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        delete ctx.request.body.id;
        let res = await faqQueries.editFaq(id, ctx.request.body);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Faq изменен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Faq не найден',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(EDIT_FAQ_TOPIC_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.EDIT_FAQ_TOPIC_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        let topic = ctx.request.body.topic;
        let res = await faqQueries.editFaqTopic(id, topic);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Топик изменен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Топик не найден',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(DELETE_FAQ_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DELETE_FAQ_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        let res = await faqQueries.deleteFaq(id);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Faq удален',
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Faq не найден',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(DELETE_FAQ_TOPIC_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DELETE_FAQ_TOPIC_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        let res = await faqQueries.deleteFaqTopic(id);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Топик удален',
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Топик не найден',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

module.exports = router;
