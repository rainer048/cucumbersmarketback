const Router = require('koa-router');
const validator = require('../helpers/validator');
const usersQueries = require('../db/queries/users');
const authHelper = require('../helpers/auth');

const router = new Router();

const PREFIX_URL = '/user';
const GET_USER_URL = `${PREFIX_URL}`;
const EDIT_PROFILE_URL = `${PREFIX_URL}/editProfile`;
const FIND_USER_URL = `${PREFIX_URL}/find`;
const GET_ALL_USERS_URL = `${PREFIX_URL}/getAllUsers`;
const BECOME_FARMER_URL =  `${PREFIX_URL}/becomeFarmer`;
const GET_FARMERS_URL =  `${PREFIX_URL}/getFarmers`;
const GET_FARMER_REQUESTS_URL = `${PREFIX_URL}/getFarmerRequests`;
const SET_IS_FARMER_URL = `${PREFIX_URL}/setIsFarmer`;

router.post(GET_USER_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_USER_SCHEMA),
    async (ctx) => {

    if (!ctx.state.user) {
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'Пользователь не получен',
            data: null,
            loggedIn: ctx.state.loggedIn
        };
        return;
    }

    try {
        let body = ctx.request.body;
        let data = Object.keys(body).length ? body : { id: ctx.state.user.id};
        let res = await usersQueries.getUser(data);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Пользователь получен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Пользователь не найден',
                loggedIn: ctx.state.loggedIn
            }
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(EDIT_PROFILE_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.EDIT_PROFILE_SCHEMA),
    async (ctx) => {

    try {
        let res = await usersQueries.editProfile(ctx.state.user.id, ctx.request.body.data);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Профиль пользователя изменен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Пользователь не найден',
                loggedIn: ctx.state.loggedIn
            }
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(FIND_USER_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.FIND_USER_SCHEMA),
    async (ctx) => {

    try {
        let searchQuery = ctx.request.body.searchQuery;
        let res = await usersQueries.findUsers(searchQuery);
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'Пользователи получены',
            data: res,
            loggedIn: ctx.state.loggedIn
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_ALL_USERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_ALL_USERS_SCHEMA),
    async (ctx) => {
        if (!ctx.state.user.isAdmin) {
            ctx.status = 403;
            ctx.body = {
                status: 'forbidden',
                message: 'Пользователь не является администратором',
                loggedIn: ctx.state.loggedIn
            };
            return;
        }
        try {
            let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
            let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
            let orderBy = ctx.request.body.orderBy ? ctx.request.body.orderBy : 'id';
            let order = ctx.request.body.order ? ctx.request.body.order : 'desc';
            let res = await usersQueries.getAllUsers(limit, offset, orderBy, order);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Пользователи получены',
                data: res.users,
                total: res.total,
                loggedIn: ctx.state.loggedIn
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(GET_FARMERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_FARMERS_SCHEMA),
    async (ctx) => {
        try {
            if (!ctx.state.user.isAdmin) {
                ctx.status = 403;
                ctx.body = {
                    status: 'forbidden',
                    message: 'Пользователь не является администратором',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            }
            let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
            let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
            let orderBy = ctx.request.body.orderBy ? ctx.request.body.orderBy : 'id';
            let order = ctx.request.body.order ? ctx.request.body.order : 'desc';
            let res = await usersQueries.getFarmers(limit, offset, orderBy, order);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Список фермеров получен',
                data: res.users,
                total: res.total,
                loggedIn: ctx.state.loggedIn
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(GET_FARMER_REQUESTS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_FARMER_REQUESTS_SCHEMA),
    async (ctx) => {
        try {
            if (!ctx.state.user.isAdmin) {
                ctx.status = 403;
                ctx.body = {
                    status: 'forbidden',
                    message: 'Пользователь не является администратором',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            }
            let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
            let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
            let orderBy = ctx.request.body.orderBy ? ctx.request.body.orderBy : 'id';
            let order = ctx.request.body.order ? ctx.request.body.order : 'desc';
            let res = await usersQueries.getFarmerRequests(limit, offset, orderBy, order);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Список заявок получен',
                data: res.users,
                total: res.total,
                loggedIn: ctx.state.loggedIn
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(SET_IS_FARMER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.SET_IS_FARMER_SCHEMA),
    async (ctx) => {
        try {
            if (!ctx.state.user.isAdmin) {
                ctx.status = 403;
                ctx.body = {
                    status: 'forbidden',
                    message: 'Пользователь не является администратором',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            }
            let userId = ctx.request.body.userId;
            let isFarmer = ctx.request.body.isFarmer;

            let res = await usersQueries.setIsFarmer(userId, isFarmer);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Статус фермера изменен',
                data: res,
                loggedIn: ctx.state.loggedIn
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(BECOME_FARMER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.SET_BECOME_FARMER_SCHEMA),
    async (ctx) => {
        try {
            let becomeFarmer = ctx.request.body.becomeFarmer;
            let userId = ctx.state.user.id;

            let res = await usersQueries.setBecomeFarmer(userId, becomeFarmer);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Успешно!',
                data: res,
                loggedIn: ctx.state.loggedIn
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });


module.exports = router;
