const Router = require('koa-router');
const registrationQueries = require('../db/queries/registration');
const validator = require('../helpers/validator');
const authHelper = require('../helpers/auth');
const PREFIX_URL = '/registration';
const USER_REGISTRATION_URL = `${PREFIX_URL}/userRegistration`;
const router = new Router();


router.post(USER_REGISTRATION_URL, validator.validate(validator.USER_REGISTRATION_SCHEMA), async (ctx) => {

    let email = ctx.request.body.email.toLowerCase();
    let telephone = ctx.request.body.telephone;
    let password = ctx.request.body.password;
    let first_name = ctx.request.body.first_name;
    let second_name = ctx.request.body.second_name;
    let subscription = ctx.request.body.subscription;

    let passwordHash = await authHelper.getPasswordHash(password);

    try {

        let user = await registrationQueries.userRegistration(email, telephone, passwordHash, first_name, second_name, subscription);
        if (!user) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера'
            };
        } else {
            let tokens = await authHelper.updateTokens(user.id);
            let cookiesConfig = authHelper.getCookiesConfig();
            ctx.cookies.set('access_token', tokens.access_token, cookiesConfig.access_token);
            ctx.cookies.set('refresh_token', tokens.refresh_token, cookiesConfig.refresh_token);
            ctx.status = 200;
            ctx.body = {
                status: 'ok',
                message: 'Пользователь зарегистрирован и аутентифицирован!',
                user: user,
                loggedIn: true
            };
        }

    } catch(err) {
        switch (err.constraint) {
            case 'users_email_unique': {
                ctx.status = 409;
                ctx.body = {
                    status: 'Conflict',
                    message: 'Пользователь с таким адресом Email уже существует!'
                };
                break;
            }
            case 'users_telephone_unique': {
                ctx.status = 409;
                ctx.body = {
                    status: 'Conflict',
                    message: 'Пользователь с таким номером телефона уже существует!'
                };
                break;
            }
            default: {
                ctx.status = 500;
                ctx.body = {
                    status: 'error',
                    message: 'Внутренняя ошибка сервера'
                };
                break;
            }
        }
    }
});


module.exports = router;
