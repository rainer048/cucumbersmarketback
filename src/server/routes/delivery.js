const Router = require('koa-router');
const validator = require('../helpers/validator');
const deliveryQueries = require('../db/queries/delivery');
const authHelper = require('../helpers/auth');
const router = new Router();
const PREFIX_URL = '/delivery';
const ADD_ADDRESS_URL = `${PREFIX_URL}/addAddress`;
const GET_MY_ADDRESSES_URL = `${PREFIX_URL}/getMyAddresses`;
const EDIT_ADDRESS_URL = `${PREFIX_URL}/editAddress`;
const DELETE_ADDRESS_URL = `${PREFIX_URL}/deleteAddress`;


router.post(ADD_ADDRESS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.ADD_ADDRESS_SCHEMA),
    async (ctx) => {

        try {
            let data = ctx.request.body;
            let res = await deliveryQueries.addAddress(data);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Адрес добавлен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(GET_MY_ADDRESSES_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_MY_ADDRESSES_SCHEMA),
    async (ctx) => {

        try {
            let res = await deliveryQueries.getMyAddresses(ctx.state.user.id);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Адреса получены',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(EDIT_ADDRESS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.EDIT_ADDRESS_SCHEMA),
    async (ctx) => {

        try {
            let data = ctx.request.body;
            let id = data.id;
            delete data.id;
            let res = await deliveryQueries.editAddress(id, data);
            if (res) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Адрес изменен',
                    data: res,
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Адрес не найден',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(DELETE_ADDRESS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DELETE_ADDRESS_SCHEMA),
    async (ctx) => {

        try {
            let id = ctx.request.body.id;
            let res = await deliveryQueries.deleteAddress(id);
            if (res) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Адрес удален',
                    data: res
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Адрес не найден',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

module.exports = router;
