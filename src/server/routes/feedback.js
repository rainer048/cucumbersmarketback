const Router = require('koa-router');
const validator = require('../helpers/validator');
const feedbackQueries = require('../db/queries/feedback');
const authHelper = require('../helpers/auth');
const router = new Router();
const PREFIX_URL = '/feedback';
const SEND_MESSAGE_URL = `${PREFIX_URL}/sendMessage`;
const GET_MESSAGES_URL = `${PREFIX_URL}/getAllMessages`;
const GET_MESSAGE_URL = `${PREFIX_URL}/getMessage`;
const DELETE_MESSAGE_URL = `${PREFIX_URL}/deleteMessage`;


router.post(GET_MESSAGE_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_MESSAGE_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        let res = await feedbackQueries.getMessage(id);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Сообщение получено',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Сообщение не найдено',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_MESSAGES_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_MESSAGES_SCHEMA),
    async (ctx) => {

    try {
        let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
        let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
        let res = await feedbackQueries.getMessages(limit, offset);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Сообщения получены',
                data: res.messages,
                total: res.total,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Сообщения не найдены',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(SEND_MESSAGE_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.SEND_MESSAGE_SCHEMA),
    async (ctx) => {

    try {
        let data = ctx.request.body;
        let res = await feedbackQueries.addMessage(data);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Сообщение отправлено',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Ошибка!',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        console.log(err)
    }
});


router.post(DELETE_MESSAGE_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DELETE_MESSAGE_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        let res = await feedbackQueries.deleteMessage(id);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Сообщение удалено',
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Сообщение не найдено',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

module.exports = router;
