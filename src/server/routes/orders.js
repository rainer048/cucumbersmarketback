const Router = require('koa-router');
const validator = require('../helpers/validator');
const ordersQueries = require('../db/queries/orders');
const marketsQueries = require('../db/queries/markets');
const authHelper = require('../helpers/auth');
const mailerHelper = require('../helpers/mailer');
const moment = require('moment');
const router = new Router();
const PREFIX_URL = '/orders';
const GET_ORDERS_URL = `${PREFIX_URL}`;
const GET_RETAIL_ORDERS_URL = `${PREFIX_URL}/getRetailOrders`;
const GET_WHOLESALE_ORDERS_URL = `${PREFIX_URL}/getWholesaleOrders`;
const GET_ORDER_URL = `${PREFIX_URL}/getOrder`;
const GET_SUBORDER_URL = `${PREFIX_URL}/getSuborder`;
const ADD_ORDER_URL = `${PREFIX_URL}/addOrder`;
const GET_MARKET_RETAIL_SUBORDERS_URL = `${PREFIX_URL}/getMarketRetailSuborders`;
const GET_MARKET_WHOLESALE_SUBORDERS_URL = `${PREFIX_URL}/getMarketWholesaleSuborders`;
const CANCEL_ORDER_URL = `${PREFIX_URL}/cancelOrder`;
const CANCEL_SUBORDER_URL = `${PREFIX_URL}/cancelSuborder`;
const SET_SUBORDER_STATUS_URL = `${PREFIX_URL}/setSuborderStatus`;
const GET_STATUS_LIST_URL = `${PREFIX_URL}/getStatusList`;


router.post(GET_ORDERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_ORDERS_SCHEMA),
    async (ctx) => {

        try {
            let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
            let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
            let status = ctx.request.body.status ? ctx.request.body.status : null;
            let userId = ctx.state.user.id;
            let orders = await ordersQueries.getOrders(userId, limit, offset, status);
            if (orders) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Заказы получены',
                    data: orders.orders,
                    total: orders.total,
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Заказы не найдены',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });


router.post(GET_RETAIL_ORDERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_ORDERS_SCHEMA),
    async (ctx) => {

        try {
            let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
            let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
            let status = ctx.request.body.status ? ctx.request.body.status : null;
            let userId = ctx.state.user.id;
            let orders = await ordersQueries.getOrders(userId, limit, offset, status, 'RETAIL');
            if (orders) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Заказы получены',
                    data: orders.orders,
                    total: orders.total,
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Заказы не найдены',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });


router.post(GET_WHOLESALE_ORDERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_ORDERS_SCHEMA),
    async (ctx) => {

        try {
            let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
            let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
            let status = ctx.request.body.status ? ctx.request.body.status : null;
            let userId = ctx.state.user.id;
            let orders = await ordersQueries.getOrders(userId, limit, offset, status, 'WHOLESALE');
            if (orders) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Заказы получены',
                    data: orders.orders,
                    total: orders.total,
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Заказы не найдены',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

let getMarketSuborders = (type) => async (ctx, next) => {
    try {
        if (!(await marketsQueries.isOwner(ctx.request.body.market, ctx.state.user.id) || (await marketsQueries.isManager(ctx.request.body.market, ctx.state.user.id)))) {
            ctx.status = 403;
            ctx.body = {
                status: 'Forbidden',
                message: 'Пользователь не является владельцем или оператором точки продажи!'
            };
            return;
        };

        let market = ctx.request.body.market;
        let fromTime = ctx.request.body.fromTime || moment().startOf('day').unix();
        let toTime = ctx.request.body.toTime || moment().endOf('day').unix();
        let limit = ctx.request.body.limit || 10;
        let offset = ctx.request.body.offset || 0;
        let orderBy = ctx.request.body.orderBy || 'created_at';
        let order = ctx.request.body.order || 'desc';
        let report = ctx.request.body.report || false;

        let suborders = report ?
            await ordersQueries.getMarketSubordersForReport(market, fromTime, toTime, limit, offset, orderBy, order, type)
            : await ordersQueries.getMarketSuborders(market, fromTime, toTime, limit, offset, orderBy, order, type);
        if (suborders) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Заказы получены',
                data: suborders.suborders || suborders[0],
                total: suborders.total || suborders[2],
                marketName: suborders[1],
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Заказы не найдены',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
    return next();
};


router.post(GET_MARKET_RETAIL_SUBORDERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_MARKET_SUBORDERS_SCHEMA),
    getMarketSuborders('RETAIL')
 );

router.post(GET_MARKET_WHOLESALE_SUBORDERS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_MARKET_SUBORDERS_SCHEMA),
    getMarketSuborders('WHOLESALE')
);


router.post(GET_ORDER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_ORDER_SCHEMA),
    async (ctx) => {

        try {
            let id = ctx.request.body.id;
            let order = await ordersQueries.getOrder(id);
            if (order) {
                if (order.customer === ctx.state.user.id) {
                    ctx.status = 200;
                    ctx.body = {
                        status: 'success',
                        message: 'Заказ получен',
                        data: order,
                        loggedIn: ctx.state.loggedIn
                    };
                } else {
                    ctx.status = 403;
                    ctx.body = {
                        status: 'forbidden',
                        message: 'Пользователь не является владельцем заказа',
                        loggedIn: ctx.state.loggedIn
                    };
                }

            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Order not found',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(GET_SUBORDER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_SUBORDER_SCHEMA),
    async (ctx) => {

        try {
            let id = ctx.request.body.id;
            let userId = ctx.state.user.id;
            let order = await ordersQueries.getOrderBySuborder(id);
            if (userId !== order.customer) {
                ctx.status = 403;
                ctx.body = {
                    status: 'forbidden',
                    message: 'Пользователь не является владельцем заказа',
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                let suborder = await ordersQueries.getSuborder(id);
                if (suborder) {
                    ctx.status = 200;
                    ctx.body = {
                        status: 'success',
                        message: 'Подзаказ получен',
                        data: suborder,
                        loggedIn: ctx.state.loggedIn
                    }
                } else {
                    ctx.status = 404;
                    ctx.body = {
                        status: 'error',
                        message: 'Подзаказ не найден!',
                        loggedIn: ctx.state.loggedIn
                    }
                }
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            }
            console.log(err)
        }
    });



router.post(ADD_ORDER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.ADD_ORDER_SCHEMA),
    async (ctx) => {

        try {
            let userId = ctx.state.user.id;
            let suborders = ctx.request.body.suborders;
            let type = ctx.request.body.type;
            let pay_type = ctx.request.body.pay_type;
            let delivery_type = ctx.request.body.delivery_type;
            let delivery_address = ctx.request.body.delivery_address || null;


            let order = await ordersQueries.addOrder(userId, suborders, type, pay_type, delivery_type, delivery_address);
            if (order) {
                await mailerHelper.sendNewOrderNotification(order);
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Заказ добавлен',
                    data: order,
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Ошибка',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            console.log(err)
        }
    });

router.post(CANCEL_ORDER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.CANCEL_ORDER_SCHEMA),
    async (ctx) => {

        let id = ctx.request.body.id;
        let userId = ctx.state.user.id;
        let isAdmin = ctx.state.user.isAdmin;
        try {
            if (!(await ordersQueries.isOrderCustomer(id, userId) || isAdmin)) {
                ctx.status = 403;
                ctx.body = {
                    status: 'Forbidden',
                    message: 'Пользователь не является владельцем заказа',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            } else {
                let order = await ordersQueries.getOrder(id);
                if (order) {
                    await mailerHelper.sendOrderIsCancelledNotification(order);
                    let newOrder = await ordersQueries.cancelOrder(id);
                    ctx.status = 200;
                    ctx.body = {
                        status: 'success',
                        message: 'Заказ отменен',
                        data: newOrder,
                        loggedIn: ctx.state.loggedIn
                    };
                } else {
                    ctx.status = 404;
                    ctx.body = {
                        status: 'error',
                        message: 'Заказ не найден',
                        loggedIn: ctx.state.loggedIn
                    };
                }
            }

        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(CANCEL_SUBORDER_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.CANCEL_SUBORDER_SCHEMA),
    async (ctx) => {

        try {
            let id = ctx.request.body.id;
            let userId = ctx.state.user.id;
            let isAdmin = ctx.state.user.isAdmin;
            if (!(await ordersQueries.isSuborderCustomer(id, userId) || isAdmin)) {
                ctx.status = 403;
                ctx.body = {
                    status: 'Forbidden',
                    message: 'Пользователь не является владельцем заказа',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            } else {
                await ordersQueries.cancelSuborder(id);
                await mailerHelper.sendSuborderIsCancelledNotification(id);
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Подзаказ отменен',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });


router.post(SET_SUBORDER_STATUS_URL,
    authHelper.checkAuthorization,
    // timeHelper.checkSetOrderStatusTime,  // редактируем только с 12 до 14
    validator.validate(validator.SET_SUBORDER_STATUS_SCHEMA),
    async (ctx) => {

        try {
            let id = ctx.request.body.id;
            let status = ctx.request.body.status;
            let userId = ctx.state.user.id;
            let isAdmin = ctx.state.user.isAdmin;
            // if (!(await ordersQueries.isTodaySuborder(id))) {
            //     ctx.status = 400;
            //     ctx.body = {
            //         status: 'Error',
            //         message: 'Разрешено изменять статусы заказов, добавленных с 12.00 прошлого дня до 12.00 текущего дня!',
            //         loggedIn: ctx.state.loggedIn
            //     };
            //     return;
            // }
            let isManager = await marketsQueries.isManagerBySuborder(id, userId);
            let isOwnerBySuborder = await marketsQueries.isOwnerBySuborder(id, userId);
            if (!(isManager || isOwnerBySuborder || isAdmin)) {
                ctx.status = 403;
                ctx.body = {
                    status: 'Forbidden',
                    message: 'Пользователь не является оператором',
                    loggedIn: ctx.state.loggedIn
                };
                return;
            } else {
                let order = await ordersQueries.setSuborderStatus(id, status);
                if (!order) {
                    ctx.status = 404;
                    ctx.body = {
                        status: 'error',
                        message: 'Заказ не найден',
                        loggedIn: ctx.state.loggedIn
                    };
                } else {
                    await mailerHelper.sendNewSuborderStatus(id);
                    ctx.status = 200;
                    ctx.body = {
                        status: 'success',
                        message: 'Статус изменен',
                        data: order,
                        loggedIn: ctx.state.loggedIn
                    };
                }
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(GET_STATUS_LIST_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_STATUS_LIST_SCHEMA),
    async (ctx) => {

        try {
            let statuses = await ordersQueries.getStatusList();
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Статусы получены',
                data: statuses,
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

module.exports = router;
