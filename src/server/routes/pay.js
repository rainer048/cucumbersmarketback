const Router = require('koa-router');
const validator = require('../helpers/validator');
const payQueries = require('../db/queries/pay');
const ordersQueries = require('../db/queries/orders');
const authHelper = require('../helpers/auth');
const router = new Router();
const PREFIX_URL = '/pay';
const GET_PAYMENT_DATA_URL = `${PREFIX_URL}/getPaymentData`;
const GET_TRANSACTION_URL = `${PREFIX_URL}/getTransaction`;
const YM_CALLBACK_URL = `${PREFIX_URL}/YMCallback`;


router.post(GET_PAYMENT_DATA_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_PAYMENT_DATA_SCHEMA),
    async (ctx) => {

        try {
            let orderId = ctx.request.body.orderId;
            let order = await ordersQueries.getOrder(orderId);
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Данные получены',
                data: {
                    'receiver': '4100110336113971',
                    'quickpay-form': 'shop',
                    'targets': `Оплата заказа №${order.id} на сайте Farmer Market`,
                    'paymentType': 'AC',
                    'sum': order.sum,
                    'formcomment': `Оплата заказа №${order.id} на сайте Farmer Market`,
                    'short-dest': `Оплата заказа №${order.id} на сайте Farmer Market`,
                    'label': order.id,
                    'comment': `Оплата заказа №${order.id} на сайте Farmer Market`
                },
                loggedIn: ctx.state.loggedIn
            };
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(YM_CALLBACK_URL,
    async (ctx) => {

        try {
            let data = ctx.request.body;
            console.log(data);
            await payQueries.addTransaction(data);
            await payQueries.changePayStatus(+data.label, +data.withdraw_amount);
            ctx.status = 200;
        } catch (err) {
            ctx.status = 500;
            console.log(err)
        }
    });

module.exports = router;
