const Router = require('koa-router');
const validator = require('../helpers/validator');
const productsQueries = require('../db/queries/products.js');
const authHelper = require('../helpers/auth');
const uploadHelper = require('../helpers/upload');
const router = new Router();
const PREFIX_URL = '/products';
const GET_ONE_URL = `${PREFIX_URL}/getOne`;
const GET_MANY_URL = `${PREFIX_URL}/getMany`;
const GET_MANY_BY_IDS_URL = `${PREFIX_URL}/getManyByIds`;
const GET_MANY_RETAIL_URL = `${PREFIX_URL}/getManyRetail`;
const GET_MANY_WHOLESALE_URL = `${PREFIX_URL}/getManyWholesale`;
const GET_BY_MARKET_ID_URL = `${PREFIX_URL}/getByMarketId`;
const GET_BY_MY_MARKETS_URL = `${PREFIX_URL}/getByMyMarkets`;
const GET_TOP_URL = `${PREFIX_URL}/getTop`;
const GET_NEW_URL = `${PREFIX_URL}/getNew`;
const GET_TOP_WHOLESALE_URL = `${PREFIX_URL}/getTopWholesale`;
const GET_NEW_WHOLESALE_URL = `${PREFIX_URL}/getNewWholesale`;
const GET_TOP_RETAIL_URL = `${PREFIX_URL}/getTopRetail`;
const GET_NEW_RETAIL_URL = `${PREFIX_URL}/getNewRetail`;
const GET_TOP_AND_NEW_URL = `${PREFIX_URL}/getTopAndNew`;
const FIND_URL = `${PREFIX_URL}/find`;
const ADD_PRODUCT_URL = `${PREFIX_URL}/add`;
const EDIT_PRODUCT_URL = `${PREFIX_URL}/edit`;

router.post(GET_ONE_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_ONE_SCHEMA),
    async (ctx) => {

    const id = ctx.request.body.id;
    try {
        let userId = ctx.state.user ? ctx.state.user.id : 0;
        let res = await productsQueries.getOneById(userId, id);
        if (res) {
            let isOwner = res.product.isOwner;
            delete res.product.isOwner;
            if (!res.product.image) res.product.image = 'img/noimage.jpg';
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Товар получен',
                data: res.product,
                canEdit: res.canEdit,
                isOwner: isOwner,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Товар не найден',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});


async function getMany(ctx, next) {

    try {
        let limit = ctx.request.body.limit ? ctx.request.body.limit : null;
        let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
        let orderBy = ctx.request.body.orderBy ? ctx.request.body.orderBy : 'name';
        let order = ctx.request.body.order ? ctx.request.body.order : 'asc';
        let category = ctx.request.body.category;
        let market = ctx.request.body.market;
        let type = ctx.state.type;
        let isAdmin = ctx.state.user ? (ctx.state.user.isAdmin && ctx.request.body.admin) : false;
        let res = (category || market) ? await productsQueries.getByParameters(limit, offset, orderBy, order, category, market, isAdmin, type)
            : await productsQueries.getMany(limit, offset, orderBy, order, isAdmin, type);
        if (res) {
            for await (product of res.products) {
                if (!product.image) product.image = 'img/noimage.jpg';
            }
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Товары получен',
                data: res.products,
                total: res.total,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Товары не найдены',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
    return next();
}

router.post(GET_MANY_RETAIL_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_MANY_SCHEMA),
    async (ctx, next) => {
        ctx.state.type = 'retail';
        return next(ctx);
    },
    getMany
);


router.post(GET_MANY_WHOLESALE_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_MANY_SCHEMA),
    async (ctx, next) => {
        ctx.state.type = 'wholesale';
        return next(ctx);
    },
    getMany
);


router.post(GET_MANY_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_MANY_SCHEMA),
    async (ctx) => {

        try {
            let limit = ctx.request.body.limit ? ctx.request.body.limit : null;
            let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
            let orderBy = ctx.request.body.orderBy ? ctx.request.body.orderBy : 'name';
            let order = ctx.request.body.order ? ctx.request.body.order : 'asc';
            let category = ctx.request.body.category;
            let market = ctx.request.body.market;
            let isAdmin = ctx.state.user ? (ctx.state.user.isAdmin && ctx.request.body.admin) : false;
            let res = (category || market) ? await productsQueries.getByParameters(limit, offset, orderBy, order, category, market, isAdmin)
                : await productsQueries.getMany(limit, offset, orderBy, order, isAdmin);
            if (res) {
                for await (product of res.products) {
                    if (!product.image) product.image = 'img/noimage.jpg';
                }
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Товары получен',
                    data: res.products,
                    total: res.total,
                    loggedIn: ctx.state.loggedIn
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Товары не найдены',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });



router.post(GET_MANY_BY_IDS_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_MANY_BY_IDS_SCHEMA),
    async (ctx) => {

        try {

            let ids = ctx.request.body.ids;

            let res = await productsQueries.getManyByIds(ids);

            if (res) {
                ctx.status = 200;
                ctx.body = {
                    status: 'success',
                    message: 'Товары получен',
                    data: res
                };
            } else {
                ctx.status = 404;
                ctx.body = {
                    status: 'error',
                    message: 'Товары не найдены',
                    loggedIn: ctx.state.loggedIn
                };
            }
        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }
    });

router.post(GET_BY_MARKET_ID_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_BY_MARKET_ID_SCHEMA),
    async (ctx) => {

    try {
        let userId = ctx.state.user ? ctx.state.user.id : 0;
        let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
        let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
        let market = ctx.request.body.market;
        let orderBy = ctx.request.body.orderBy ? ctx.request.body.orderBy : 'id';
        let order = ctx.request.body.order ? ctx.request.body.order : 'desc';
            let res = await productsQueries.getByMarketId(userId, market, limit, offset, orderBy, order);
        if (res) {
            for await (product of res.products) {
                if (!product.image) product.image = 'img/noimage.jpg';
            };
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Товары получены',
                data: res.products,
                total: res.total,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Товары не найдены',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_BY_MY_MARKETS_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_BY_MY_MARKETS_SCHEMA),
    async (ctx) => {

    try {
        let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
        let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
        let orderBy = ctx.request.body.orderBy ? ctx.request.body.orderBy : 'id';
        let order = ctx.request.body.order ? ctx.request.body.order : 'desc';
        let market = ctx.request.body.market ? ctx.request.body.market : null;
        let owner = ctx.state.user.id;
        let onlyActive = ctx.request.body.onlyActive ? ctx.request.body.onlyActive : false;
        let res = await productsQueries.getByMyMarkets(owner, limit, offset, orderBy, order, market, onlyActive);

        if (res) {
            for await (product of res.products) {
                if (!product.image) product.image = 'img/noimage.jpg';
            };
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Товары получены',
                data: res.products,
                total: res.total,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Товары не найдены',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

async function getNew(ctx, next) {

    try {
        let products = await productsQueries.getNew(ctx.state.type);
        for await (product of products) {
            if (!product.image) product.image = 'img/noimage.jpg';
        }
        ;
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'Товары получены',
            data: products,
            loggedIn: ctx.state.loggedIn
        };
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }

    return next(ctx);
}

async function getTop(ctx, next) {

    try {
        let products = await productsQueries.getTop(ctx.state.type);
        for await (product of products) {
            if (!product.image) product.image = 'img/noimage.jpg';
        }
        ctx.status = 200;
        ctx.body = {
            status: 'success',
            message: 'Товары получены',
            data: products,
            loggedIn: ctx.state.loggedIn
        };
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }

    return next(ctx);
}

router.post(GET_TOP_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_TOP_SCHEMA),
    getTop
);

router.post(GET_NEW_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_NEW_SCHEMA),
    getNew
);

router.post(GET_TOP_RETAIL_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_TOP_SCHEMA),
    async (ctx, next) => {
        ctx.state.type = 'retail';
        return next();
    },
    getTop
);

router.post(GET_NEW_RETAIL_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_NEW_SCHEMA),
    async (ctx, next) => {
        ctx.state.type = 'retail';
        return next();
    },
    getNew
);

router.post(GET_TOP_WHOLESALE_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_TOP_SCHEMA),
    async (ctx, next) => {
        ctx.state.type = 'wholesale';
        return next();
    },
    getTop
);

router.post(GET_NEW_WHOLESALE_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_NEW_SCHEMA),
    async (ctx, next) => {
        ctx.state.type = 'wholesale';
        return next();
    },
    getNew
);

router.post(GET_TOP_AND_NEW_URL,
    authHelper.checkLoggedIn,
    async (ctx, next) => {

        try {
            let topRetail = await productsQueries.getTop('retail');
            let newRetail = await productsQueries.getNew('retail');
            let topWholesale = await productsQueries.getTop('wholesale');
            let newWholesale = await productsQueries.getNew('wholesale');

            for await (product of topRetail) {
                if (!product.image) product.image = 'img/noimage.jpg';
            }
            for await (product of newRetail) {
                if (!product.image) product.image = 'img/noimage.jpg';
            }
            for await (product of topWholesale) {
                if (!product.image) product.image = 'img/noimage.jpg';
            }
            for await (product of newWholesale) {
                if (!product.image) product.image = 'img/noimage.jpg';
            }

            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Товары получены',
                data: {
                    topRetail: topRetail,
                    newRetail: newRetail,
                    topWholesale: topWholesale,
                    newWholesale: newWholesale
                },
                loggedIn: ctx.state.loggedIn
            };

        } catch (err) {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err)
        }

        return next(ctx);
    }

);


router.post(FIND_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.FIND_SCHEMA),
    async (ctx) => {

    try {
        let limit = ctx.request.body.limit ? ctx.request.body.limit : 10;
        let offset = ctx.request.body.offset ? ctx.request.body.offset : 0;
        let orderBy = ctx.request.body.orderBy ? ctx.request.body.orderBy : 'id';
        let order = ctx.request.body.order ? ctx.request.body.order : 'desc';
        let type = ctx.request.body.type;
        let searchQuery = ctx.request.body.searchQuery;
        let res = await productsQueries.find(searchQuery, limit, offset, orderBy, order, type);
        if (res) {
            for await (product of res.products) {
                if (!product.image) product.image = 'img/noimage.jpg';
            };
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Товары получены',
                data: res.products,
                total: res.total,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 500;
            ctx.body = {
                status: 'error',
                message: 'Внутренняя ошибка сервера.',
                loggedIn: ctx.state.loggedIn
            };
            console.log(err);
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});


router.post(ADD_PRODUCT_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.ADD_PRODUCT_SCHEMA),
    uploadHelper.addImage, async (ctx) => {

    try {

        let res = await productsQueries.addProduct(ctx.request.body);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Товар добавлен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Товар не найден',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(EDIT_PRODUCT_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.EDIT_PRODUCT_SCHEMA),
    uploadHelper.updateImage, async (ctx) => {

    try {
        let id = ctx.request.body.id;
        delete ctx.request.body.id;
        let res = await productsQueries.editProduct(id, ctx.request.body);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Товар изменен',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Товар не найден',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});


module.exports = router;
