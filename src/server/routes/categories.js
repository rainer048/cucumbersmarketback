const Router = require('koa-router');
const validator = require('../helpers/validator');
const categoriesQueries = require('../db/queries/categories');
const authHelper = require('../helpers/auth');
const router = new Router();
const PREFIX_URL = '/categories';
const GET_ALL_CATEGORIES_URL = `${PREFIX_URL}`;
const ADD_CATEGORY_URL = `${PREFIX_URL}/add`;
const GET_ONE_CATEGORY_URL = `${PREFIX_URL}/getOne`;
const EDIT_CATEGORY_URL = `${PREFIX_URL}/edit`;
const DELETE_CATEGORY_URL = `${PREFIX_URL}/delete`;


router.post(GET_ALL_CATEGORIES_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.GET_ALL_CATEGORIES_SCHEMA),
    async (ctx) => {
    let forEdit = ctx.request.body.forEdit || false;
    try {
        let res = await categoriesQueries.getAllCategories(forEdit);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Категории отправлены',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Категории не найдены',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(GET_ONE_CATEGORY_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.GET_ONE_CATEGORY_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        let res = await categoriesQueries.getOne(id);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Категории отправлены',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Категории не найдены',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(ADD_CATEGORY_URL,
    authHelper.checkLoggedIn,
    validator.validate(validator.ADD_CATEGORY_SCHEMA),
    async (ctx) => {

    try {
        let name = ctx.request.body.name;
        let res = await categoriesQueries.addCategory(name);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Категория добавлена',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

router.post(EDIT_CATEGORY_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.EDIT_CATEGORY_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        delete ctx.request.body.id;
        let res = await categoriesQueries.editCategory(id, ctx.request.body);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Категория изменена',
                data: res,
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Категория не найдена',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});


router.post(DELETE_CATEGORY_URL,
    authHelper.checkAuthorization,
    validator.validate(validator.DELETE_CATEGORY_SCHEMA),
    async (ctx) => {

    try {
        let id = ctx.request.body.id;
        let res = await categoriesQueries.deleteCategory(id);
        if (res) {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                message: 'Категория удалена',
                loggedIn: ctx.state.loggedIn
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'Категория не найдена',
                loggedIn: ctx.state.loggedIn
            };
        }
    } catch (err) {
        ctx.status = 500;
        ctx.body = {
            status: 'error',
            message: 'Внутренняя ошибка сервера.',
            loggedIn: ctx.state.loggedIn
        };
        console.log(err)
    }
});

module.exports = router;
