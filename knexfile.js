const path = require('path');

const BASE_PATH = path.join(__dirname, 'src', 'server', 'db');

module.exports = {
    test: {
        client: 'pg',
        connection: 'postgres://postgresadmin:PostgresAdmin@localhost:5432/farmer_market_db',
        migrations: {
            directory: path.join(BASE_PATH, 'migrations')
        },
        seeds: {
            directory: path.join(BASE_PATH, 'seeds')
        }
    },
    development: {
        client: 'pg',
        connection: 'postgres://postgresadmin:PostgresAdmin@localhost:5432/farmer_market_db',
        migrations: {
            directory: path.join(BASE_PATH, 'migrations')
        },
        seeds: {
            directory: path.join(BASE_PATH, 'seeds')
        }
    },
    onUpdateTrigger: table => `
        CREATE TRIGGER ${table}_updated_at
        BEFORE UPDATE ON ${table}
        FOR EACH ROW
        EXECUTE PROCEDURE on_update_timestamp();
    `,
    onUpdateManagersTrigger: table => `
        CREATE TRIGGER ${table}_update
        AFTER INSERT OR UPDATE ON ${table}
        FOR EACH ROW
        EXECUTE PROCEDURE on_update_managers();
    `,
    onUpdateCouriersTrigger: table => `
        CREATE TRIGGER ${table}_update
        AFTER INSERT OR UPDATE ON ${table}
        FOR EACH ROW
        EXECUTE PROCEDURE on_update_couriers();
    `
};
